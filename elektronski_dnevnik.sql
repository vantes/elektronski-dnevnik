-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: db
-- Generation Time: Nov 28, 2020 at 12:18 AM
-- Server version: 10.4.12-MariaDB-1:10.4.12+maria~bionic
-- PHP Version: 7.4.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `gavra11`
--

-- --------------------------------------------------------

--
-- Table structure for table `assigned_subjects`
--

CREATE TABLE `assigned_subjects` (
  `id` int(11) NOT NULL,
  `teacher_id` int(11) NOT NULL,
  `subject_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `assigned_subjects`
--

INSERT INTO `assigned_subjects` (`id`, `teacher_id`, `subject_id`) VALUES
(1, 1, 2),
(2, 1, 3),
(3, 2, 3),
(4, 3, 1),
(5, 4, 4),
(6, 4, 5),
(7, 7, 6);

-- --------------------------------------------------------

--
-- Table structure for table `final_mark`
--

CREATE TABLE `final_mark` (
  `id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL,
  `subject_id` int(11) NOT NULL,
  `teacher_id` int(11) NOT NULL,
  `final_mark` int(1) NOT NULL,
  `explanation` text DEFAULT NULL,
  `date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE current_timestamp(),
  `school_year` int(11) NOT NULL,
  `deleted` tinyint(1) UNSIGNED NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `final_mark`
--

INSERT INTO `final_mark` (`id`, `student_id`, `subject_id`, `teacher_id`, `final_mark`, `explanation`, `date`, `school_year`, `deleted`) VALUES
(1, 1, 1, 3, 4, 'Zbog necega', '2020-07-26 20:54:56', 1, 0),
(7, 3, 2, 1, 5, 'Isticao se na predavanjima i vežbama.', '2020-11-27 13:32:29', 1, 0),
(8, 1, 2, 1, 5, 'Učenik je ispravio predhodnu dvojku i izvanredno uradio godišnji test.', '2020-11-27 17:34:27', 1, 0),
(9, 2, 2, 1, 3, '', '2020-11-27 17:46:49', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `mark`
--

CREATE TABLE `mark` (
  `id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL,
  `subject_id` int(11) NOT NULL,
  `teacher_id` int(11) NOT NULL,
  `school_year` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `mark` int(1) NOT NULL,
  `quarter` int(1) UNSIGNED NOT NULL,
  `deleted` tinyint(1) UNSIGNED DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `mark`
--

INSERT INTO `mark` (`id`, `student_id`, `subject_id`, `teacher_id`, `school_year`, `created_at`, `mark`, `quarter`, `deleted`) VALUES
(1, 1, 1, 3, 1, '2018-11-06 04:25:31', 3, 1, 0),
(2, 1, 1, 3, 1, '2020-03-07 17:55:45', 4, 1, 0),
(3, 1, 1, 3, 1, '2020-03-07 17:55:45', 4, 1, 0),
(4, 1, 2, 1, 1, '2020-03-07 17:52:46', 4, 2, 0),
(5, 1, 2, 1, 1, '2020-03-07 17:44:24', 4, 2, 0),
(6, 2, 5, 4, 1, '2019-01-17 11:50:11', 5, 2, 0),
(7, 4, 1, 3, 1, '2020-02-17 19:19:37', 2, 2, 0),
(8, 4, 1, 3, 1, '2020-02-17 00:00:00', 5, 1, 0),
(9, 4, 1, 3, 1, '2020-02-16 00:00:00', 2, 1, 0),
(15, 4, 1, 3, 1, '2020-02-18 19:48:08', 2, 2, 0),
(16, 4, 1, 3, 1, '2020-02-18 19:48:24', 5, 2, 0),
(17, 4, 1, 3, 1, '2020-02-18 19:48:26', 5, 2, 0),
(18, 4, 1, 3, 1, '2020-02-18 19:48:28', 5, 2, 0),
(19, 4, 1, 3, 1, '2020-02-18 19:49:36', 1, 2, 0),
(20, 4, 1, 3, 1, '2020-02-18 19:50:17', 3, 1, 0),
(21, 1, 1, 3, 1, '2020-02-18 19:59:45', 1, 1, 0),
(22, 1, 1, 3, 1, '2020-02-19 19:17:14', 3, 1, 0),
(23, 4, 1, 3, 1, '2020-02-19 19:27:30', 2, 1, 0),
(24, 4, 1, 3, 1, '2020-02-19 20:24:32', 1, 3, 0),
(25, 4, 1, 3, 1, '2020-02-19 20:24:33', 2, 4, 0),
(26, 4, 1, 3, 1, '2020-02-19 19:31:25', 3, 1, 0),
(27, 4, 1, 3, 1, '2020-02-19 21:25:16', 2, 1, 0),
(28, 2, 1, 3, 1, '2020-07-26 20:12:54', 4, 3, 0),
(29, 2, 1, 3, 1, '2020-07-26 20:13:26', 5, 4, 0),
(30, 3, 2, 1, 1, '2020-11-27 13:27:47', 4, 4, 0),
(31, 3, 2, 1, 1, '2020-11-27 13:28:56', 3, 4, 0),
(32, 3, 2, 1, 1, '2020-11-27 13:29:02', 4, 4, 0),
(33, 3, 2, 1, 1, '2020-11-27 13:29:08', 4, 4, 0),
(34, 3, 3, 2, 1, '2020-11-27 13:39:46', 2, 4, 0),
(35, 2, 1, 3, 1, '2020-11-27 13:40:58', 5, 4, 0),
(36, 1, 4, 4, 1, '2020-11-27 13:42:12', 5, 4, 0),
(37, 1, 2, 1, 1, '2020-11-27 16:03:18', 2, 4, 0),
(38, 1, 2, 1, 1, '2020-11-27 17:13:34', 4, 4, 0),
(39, 2, 2, 1, 1, '2020-11-27 17:45:43', 3, 4, 0),
(40, 2, 2, 1, 1, '2020-11-27 17:45:47', 2, 4, 0),
(41, 2, 2, 1, 1, '2020-11-27 17:45:53', 1, 4, 0),
(42, 2, 2, 1, 1, '2020-11-27 17:45:59', 4, 4, 0);

-- --------------------------------------------------------

--
-- Table structure for table `school_year`
--

CREATE TABLE `school_year` (
  `id` int(11) NOT NULL,
  `start_of_first_quarter` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `end_of_first_quarter` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `start_of_second_quarter` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `end_of_second_quarter` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `start_of_third_quarter` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `end_of_third_quarter` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `start_of_fourth_quarter` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `end_of_fourth_quarter` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted` tinyint(1) UNSIGNED NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `school_year`
--

INSERT INTO `school_year` (`id`, `start_of_first_quarter`, `end_of_first_quarter`, `start_of_second_quarter`, `end_of_second_quarter`, `start_of_third_quarter`, `end_of_third_quarter`, `start_of_fourth_quarter`, `end_of_fourth_quarter`, `deleted`) VALUES
(1, '2019-09-01 00:00:00', '2019-12-31 00:00:00', '2020-01-01 00:00:00', '2020-03-01 00:00:00', '2020-03-01 00:00:00', '2020-06-01 00:00:00', '2020-06-01 00:00:00', '2020-12-31 00:00:00', 0);

-- --------------------------------------------------------

--
-- Table structure for table `student`
--

CREATE TABLE `student` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `forename` varchar(30) NOT NULL,
  `surname` varchar(50) NOT NULL,
  `address` text NOT NULL,
  `phone` varchar(30) NOT NULL,
  `division` text NOT NULL,
  `year` int(1) NOT NULL,
  `gender` varchar(1) NOT NULL,
  `deleted` tinyint(1) UNSIGNED NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `student`
--

INSERT INTO `student` (`id`, `user_id`, `forename`, `surname`, `address`, `phone`, `division`, `year`, `gender`, `deleted`) VALUES
(1, 9, 'Pera', 'Perić', 'adresa 12', '2314465456', 'informatika', 1, 'M', 0),
(2, 10, 'Žika', 'Žikić', 'adresa 2', '465654', 'informatika', 3, 'M', 0),
(3, 11, 'Branko', 'Branković', 'adresa 3', '89789654', 'opšti', 1, 'M', 0),
(4, 12, 'Marko', 'Marković', 'adresa 4', '231346588', 'opšti', 1, 'M', 0),
(6, 22, 'Jelena', 'Jelenković', 'test test test', '65489125', 'opšti', 2, 'F', 0);

-- --------------------------------------------------------

--
-- Table structure for table `student_listens`
--

CREATE TABLE `student_listens` (
  `id` int(11) NOT NULL,
  `teacher_id` int(11) NOT NULL,
  `subject_id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `student_listens`
--

INSERT INTO `student_listens` (`id`, `teacher_id`, `subject_id`, `student_id`) VALUES
(1, 3, 1, 1),
(2, 3, 1, 2),
(3, 3, 1, 3),
(4, 3, 1, 4),
(5, 1, 2, 1),
(6, 1, 2, 2),
(7, 1, 2, 3),
(8, 1, 2, 4),
(9, 1, 3, 4),
(10, 2, 3, 3),
(11, 4, 4, 1),
(12, 4, 5, 2),
(14, 7, 6, 6);

-- --------------------------------------------------------

--
-- Table structure for table `subject`
--

CREATE TABLE `subject` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `description` text NOT NULL,
  `deleted` tinyint(1) UNSIGNED NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `subject`
--

INSERT INTO `subject` (`id`, `name`, `description`, `deleted`) VALUES
(1, 'Srpski jezik i književnost', 'Proširivanje i produbljivanje znanja o srpskom književnom jeziku. Razvijanje i negovanje jezičke kulture, poštovanje pravila književnog (standardnog) jezika u usmenom i pismenom izrazavanju.', 0),
(2, 'Matematika', 'Uvod u: 1.Geometriju; 2.Logika i skupovi; 3.Realni brojevi; 4.Realni brojevi; 5.Proporcionalnost; 6.Racionalni algebarski izrazi; 7.Linearne jednačine, nejednačine, sistemi, linearna funkcija', 0),
(3, 'Fizika', 'Fizika je osnovna prirodna nauka koja proučava osnovna ili suštinska svojstva prirodnih pojava i tela. Fizicari proučavaju osnovna svojstva, strukturu i kretanje materije u prostoru i vremenu.', 0),
(4, 'Informatika 1', 'Računarska nauka koja se bavi strukturom i automatskom (mašinskom) obradom podataka, njihovim implementacijama i primenama u računarskim sistemima.', 0),
(5, 'Informatika 2', 'Nastavak informatike 1', 0),
(6, 'Istorija', 'Proučava istoriju kao nauku, istorijske izvore, istorijsko istraživanje, praistoriju čoveka itd.', 0);

-- --------------------------------------------------------

--
-- Table structure for table `teacher`
--

CREATE TABLE `teacher` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `forename` varchar(30) NOT NULL,
  `surname` varchar(50) NOT NULL,
  `qualifications` text NOT NULL,
  `gender` varchar(1) NOT NULL,
  `deleted` tinyint(1) UNSIGNED NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `teacher`
--

INSERT INTO `teacher` (`id`, `user_id`, `forename`, `surname`, `qualifications`, `gender`, `deleted`) VALUES
(1, 5, 'Nenad', 'Nenadović', 'da predaje matematiku i fiziku', 'M', 0),
(2, 6, 'Petar', 'Petrović', 'da predaje fiziku', 'M', 0),
(3, 7, 'Zorica', 'Zoranović', 'da predaje srpski jezik i književnost', 'F', 0),
(4, 8, 'Mira', 'Mirković', 'da predaje informatiku 1 i informatiku 2', 'F', 0),
(7, 23, 'Stevan', 'Stevanović', 'da predaje istoriju', 'M', 0);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `uid` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE current_timestamp(),
  `email` varchar(255) NOT NULL,
  `password_hash` varchar(128) NOT NULL,
  `type` varchar(32) NOT NULL,
  `deleted` tinyint(1) UNSIGNED NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`uid`, `created_at`, `email`, `password_hash`, `type`, `deleted`) VALUES
(1, '2020-11-27 21:12:28', 'admin@test.rs', '8e7c24c3dbdd9b538ea4f5dec515c607f2d12ffd931a823b86a284f3f15ce7608ded866a471569fd0605b8b9d778823429f56bf97a2b0dfd700c80ee83b1151c', 'admin', 0),
(5, '2020-03-07 17:43:37', 'nenad.nenadovic@test.rs', '8e7c24c3dbdd9b538ea4f5dec515c607f2d12ffd931a823b86a284f3f15ce7608ded866a471569fd0605b8b9d778823429f56bf97a2b0dfd700c80ee83b1151c', 'teacher', 0),
(6, '2020-03-07 17:05:33', 'petar.petrovic@test.rs', '8e7c24c3dbdd9b538ea4f5dec515c607f2d12ffd931a823b86a284f3f15ce7608ded866a471569fd0605b8b9d778823429f56bf97a2b0dfd700c80ee83b1151c', 'teacher', 0),
(7, '2020-03-07 17:05:33', 'zorica.zoranovic@test.rs', '8e7c24c3dbdd9b538ea4f5dec515c607f2d12ffd931a823b86a284f3f15ce7608ded866a471569fd0605b8b9d778823429f56bf97a2b0dfd700c80ee83b1151c', 'teacher', 0),
(8, '2020-03-07 17:05:33', 'mira.mirkovic@test.rs', '8e7c24c3dbdd9b538ea4f5dec515c607f2d12ffd931a823b86a284f3f15ce7608ded866a471569fd0605b8b9d778823429f56bf97a2b0dfd700c80ee83b1151c', 'teacher', 0),
(9, '2020-03-07 17:05:33', 'pera.peric@test.rs', '8e7c24c3dbdd9b538ea4f5dec515c607f2d12ffd931a823b86a284f3f15ce7608ded866a471569fd0605b8b9d778823429f56bf97a2b0dfd700c80ee83b1151c', 'student', 0),
(10, '2020-03-07 17:05:33', 'zika.zikic@test.rs', '8e7c24c3dbdd9b538ea4f5dec515c607f2d12ffd931a823b86a284f3f15ce7608ded866a471569fd0605b8b9d778823429f56bf97a2b0dfd700c80ee83b1151c', 'student', 0),
(11, '2020-03-07 17:05:33', 'branko.brankovic@test.rs', '8e7c24c3dbdd9b538ea4f5dec515c607f2d12ffd931a823b86a284f3f15ce7608ded866a471569fd0605b8b9d778823429f56bf97a2b0dfd700c80ee83b1151c', 'student', 0),
(12, '2020-03-07 17:05:33', 'marko.markovic@test.rs', '8e7c24c3dbdd9b538ea4f5dec515c607f2d12ffd931a823b86a284f3f15ce7608ded866a471569fd0605b8b9d778823429f56bf97a2b0dfd700c80ee83b1151c', 'student', 0),
(21, '2020-11-27 20:33:59', 'admin.drugi@test.rs', '8e7c24c3dbdd9b538ea4f5dec515c607f2d12ffd931a823b86a284f3f15ce7608ded866a471569fd0605b8b9d778823429f56bf97a2b0dfd700c80ee83b1151c', 'admin', 0),
(22, '2020-11-27 22:18:22', 'jelena.jelenkovic@test.rs', '8e7c24c3dbdd9b538ea4f5dec515c607f2d12ffd931a823b86a284f3f15ce7608ded866a471569fd0605b8b9d778823429f56bf97a2b0dfd700c80ee83b1151c', 'student', 0),
(23, '2020-11-27 23:53:48', 'stevan.stevanovic@test.rs', '8e7c24c3dbdd9b538ea4f5dec515c607f2d12ffd931a823b86a284f3f15ce7608ded866a471569fd0605b8b9d778823429f56bf97a2b0dfd700c80ee83b1151c', 'teacher', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `assigned_subjects`
--
ALTER TABLE `assigned_subjects`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_assigned_subjects_subject_id` (`subject_id`),
  ADD KEY `fk_assigned_subjects_teacher_id` (`teacher_id`);

--
-- Indexes for table `final_mark`
--
ALTER TABLE `final_mark`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQUE_FINAL_MARK_KEY` (`student_id`,`subject_id`,`teacher_id`),
  ADD KEY `fk_final_mark_student_id` (`student_id`),
  ADD KEY `fk_final_mark_subject_id` (`subject_id`),
  ADD KEY `fk_final_mark_teacher_id` (`teacher_id`),
  ADD KEY `fk_final_mark_school_year_id` (`school_year`);

--
-- Indexes for table `mark`
--
ALTER TABLE `mark`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_marks_check_quarter_in_year` (`school_year`),
  ADD KEY `fk_marks_student_id` (`student_id`),
  ADD KEY `fk_marks_subject_id` (`subject_id`),
  ADD KEY `fk_marks_teacher_id` (`teacher_id`);

--
-- Indexes for table `school_year`
--
ALTER TABLE `school_year`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `student`
--
ALTER TABLE `student`
  ADD PRIMARY KEY (`id`),
  ADD KEY `STUDENT_USER_ID` (`user_id`);

--
-- Indexes for table `student_listens`
--
ALTER TABLE `student_listens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_student_listens_student_id` (`student_id`),
  ADD KEY `fk_student_listens_subject_id` (`subject_id`),
  ADD KEY `fk_student_listens_teacher_id` (`teacher_id`);

--
-- Indexes for table `subject`
--
ALTER TABLE `subject`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `teacher`
--
ALTER TABLE `teacher`
  ADD PRIMARY KEY (`id`),
  ADD KEY `TEACHER_USER_ID` (`user_id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`uid`),
  ADD UNIQUE KEY `email` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `assigned_subjects`
--
ALTER TABLE `assigned_subjects`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `final_mark`
--
ALTER TABLE `final_mark`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `mark`
--
ALTER TABLE `mark`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;

--
-- AUTO_INCREMENT for table `school_year`
--
ALTER TABLE `school_year`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `student`
--
ALTER TABLE `student`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `student_listens`
--
ALTER TABLE `student_listens`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `subject`
--
ALTER TABLE `subject`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `teacher`
--
ALTER TABLE `teacher`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `uid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `assigned_subjects`
--
ALTER TABLE `assigned_subjects`
  ADD CONSTRAINT `fk_assigned_subjects_subject_id` FOREIGN KEY (`subject_id`) REFERENCES `subject` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_assigned_subjects_teacher_id` FOREIGN KEY (`teacher_id`) REFERENCES `teacher` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `final_mark`
--
ALTER TABLE `final_mark`
  ADD CONSTRAINT `fk_final_mark_school_year_id` FOREIGN KEY (`school_year`) REFERENCES `school_year` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_final_mark_student_id` FOREIGN KEY (`student_id`) REFERENCES `student` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_final_mark_subject_id` FOREIGN KEY (`subject_id`) REFERENCES `subject` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_final_mark_teacher_id` FOREIGN KEY (`teacher_id`) REFERENCES `teacher` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `mark`
--
ALTER TABLE `mark`
  ADD CONSTRAINT `fk_marks_check_quarter_in_year` FOREIGN KEY (`school_year`) REFERENCES `school_year` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_marks_student_id` FOREIGN KEY (`student_id`) REFERENCES `student` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_marks_subject_id` FOREIGN KEY (`subject_id`) REFERENCES `subject` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_marks_teacher_id` FOREIGN KEY (`teacher_id`) REFERENCES `teacher` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `student`
--
ALTER TABLE `student`
  ADD CONSTRAINT `STUDENT_USER_ID` FOREIGN KEY (`user_id`) REFERENCES `user` (`uid`) ON DELETE CASCADE;

--
-- Constraints for table `student_listens`
--
ALTER TABLE `student_listens`
  ADD CONSTRAINT `fk_student_listens_student_id` FOREIGN KEY (`student_id`) REFERENCES `student` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_student_listens_subject_id` FOREIGN KEY (`subject_id`) REFERENCES `subject` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_student_listens_teacher_id` FOREIGN KEY (`teacher_id`) REFERENCES `teacher` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `teacher`
--
ALTER TABLE `teacher`
  ADD CONSTRAINT `TEACHER_USER_ID` FOREIGN KEY (`user_id`) REFERENCES `user` (`uid`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
