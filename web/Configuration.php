<?php declare(strict_types = 1);

namespace App;

use App\Core\Session\FileSessionStorage;

final class Configuration {

    const BASE = 'http://elektronski-dnevnik.docker';

    const DATABASE_HOST = 'db';
    const DATABASE_NAME = 'gavra11';
    const DATABASE_USER = 'gavra11';
    const DATABASE_PASS = 'gavra11';

    const SESSION_STORAGE = FileSessionStorage::class;
    const SESSION_STORAGE_DATA = [ __DIR__ . '/sessions/' ];
    const SESSION_LIFETIME = 3600;
}