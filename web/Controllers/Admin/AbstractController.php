<?php declare(strict_types = 1);

namespace App\Controllers\Admin;

use App\Configuration;
use App\Core\PageResult;
use App\Models\Data\EntityInterface;
use App\Models\View\AdminViewModel;
use App\Models\Data\DataCollection;
use App\Models\View\AdminViewConfiguration;
use App\Services\MessageManager;

/**
 * Base admin CRUD controller
 */
abstract class AbstractController extends \App\Core\Controller {

    const LIST_VIEW_TEMPLATE = "listView";
    const EDIT_VIEW_TEMPLATE = "editView";
    const CREATE_VIEW_TEMPLATE = "editView";

    /**
     * @return AdminViewModel|null
     */
    protected function getAdminViewModel(): ?AdminViewModel {
        $userData = $this->getSession()->get('user_data');
        $adminViewModel = new AdminViewModel($this->getViewConfiguration());
        $adminViewModel->loadDataModel($userData->uid);

        return $adminViewModel;
    }

    protected abstract function getViewConfiguration(): AdminViewConfiguration;

    public function __pre() {
        if(!$this->checkAccessByType(['admin'])) {
            $this->messageManager->addMessage(MessageManager::MESSAGE_TYPE_ERROR, 'Nemate pravo pristupa ovoj stranici');
            $this->redirect(Configuration::BASE);
        }
    }

    /**
     * Handles entity listings, including pagination
     *
     * @return PageResult|null
     */
    public function listView(): ?PageResult {
        $adminViewModel = $this->getAdminViewModel();

        $params = [
            'filters' => [],
            'page' => intval($this->getRequest()->getParam('page', 0)),
            'limit' => 5
        ];

        $list = $this->getList($params);
        $adminViewModel->setActiveList($list);

        return new PageResult(self::LIST_VIEW_TEMPLATE, $adminViewModel);
    }

    /**
     * Handles entity edit / create view
     *
     * @return PageResult|null
     */
    public function editView(): ?PageResult {
        $adminViewModel = $this->getAdminViewModel();

        $entityId = intval($this->getRequest()->getParam('id'));

        $entity = $this->getEntity($entityId);
        $adminViewModel->setActiveEntity($entity);

        return new PageResult(self::EDIT_VIEW_TEMPLATE, $adminViewModel);
    }

    /**
     * Returns entity being created / edited
     *
     * @param integer $id
     * @return EntityInterface
     */
    public abstract function getEntity(?int $id): EntityInterface;

    /**
     * Returns entity array given parameters
     *
     * @param array $params
     * @return DataCollection
     */
    public abstract function getList(array $params): DataCollection;

    /**
     * Handles entity edit / create view
     *
     * @return PageResult|null
     */
    public function createView(): ?PageResult {
        return $this->editView();
    }

    /**
     * Handles entity save request
     *
     * @return void
     */
    public abstract function savePost();

    /**
     * Handles entity delete request
     *
     * @return void
     */
    public abstract function deletePost();

}