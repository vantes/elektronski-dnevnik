<?php declare(strict_types = 1);

namespace App\Controllers\Admin;

use App\Services\ServiceManager;
use App\Core\DatabaseConnection;
use App\Core\PageResult;
use App\Core\Session\Session;
use App\Exceptions\ValidationException;
use App\Models\Data\DataCollection;
use App\Services\MessageManager;
use App\Services\Repository\MarkRepository;
use App\Models\Data\EntityInterface;
use App\Models\Data\MarkData;
use App\Models\View\AdminViewConfiguration;
use App\Services\Repository\StudentRepository;
use App\Services\Repository\TeacherRepository;
use App\Validators\MarkValidator;

class MarkController extends AbstractController {

    const LIST_URL = "/admin/mark";
    const VIEW_URL = "/admin/mark/view";
    const EDIT_URL = "/admin/mark/edit";
    const SAVE_URL = "/admin/mark/savePost";
    const DELETE_URL = "/admin/mark/deletePost";
    const RESTORE_URL = "/admin/mark/restorePost";

    /** @var MarkRepository */
    private $markRepository;

    /** @var StudentRepository */
    private $studentRepository;

    /** @var TeacherRepository */
    private $teacherRepository;

    /** @var MarkValidator */
    private $markValidator;

    public function __construct(
        DatabaseConnection $dbc,
        Session $session,
        MessageManager $messageManager
    ) {
        parent::__construct($dbc, $session, $messageManager);
        $this->markRepository = ServiceManager::getService(MarkRepository::class);
        $this->studentRepository = ServiceManager::getService(StudentRepository::class);
        $this->teacherRepository = ServiceManager::getService(TeacherRepository::class);
        $this->markValidator = ServiceManager::getService(MarkValidator::class);
    }

    /**
     * @inheritDoc
     *
     * @return AdminViewConfiguration
     */
    protected function getViewConfiguration(): AdminViewConfiguration {
        $viewConfiguration = new AdminViewConfiguration();
        $viewConfiguration->setViewUrl(self::VIEW_URL);
        $viewConfiguration->setEditUrl(self::EDIT_URL);
        $viewConfiguration->setSaveUrl(self::SAVE_URL);
        $viewConfiguration->setDeleteUrl(self::DELETE_URL);
        $viewConfiguration->setRestoreUrl(self::RESTORE_URL);
        return $viewConfiguration;
    }

    /**
     * @inheritDoc
     *
     * @param integer $id
     * @return EntityInterface
     */
    public function getEntity(?int $id): EntityInterface {
        if ($id) {
            return $this->markRepository->getById($id);
        }
        return new MarkData();
    }

    /**
     * @inheritDoc
     *
     * @param array $params
     * @return array
     */
    public function getList(array $params, bool $applyDefaultFilters = false): DataCollection {
        $items =  $this->markRepository->search($params['filters'], $params['page'], $params['limit'], $applyDefaultFilters);
        $count =  $this->markRepository->count($params['filters'], $applyDefaultFilters);

        $collection = new DataCollection();
        $collection->setItems($items);
        $collection->setPage($params['page']);
        $collection->setPageSize($params['limit']);
        $collection->setSize($count);

        return $collection;
    }

    /**
     * Handles entity edit / create view
     *
     * @return PageResult|null
     */
    public function editView(): ?PageResult {
        $adminViewModel = $this->getAdminViewModel();

        $entityId = intval($this->getRequest()->getParam('id'));

        /** @var MarkData $entity */
        $entity = $this->getEntity($entityId);

        $student = $this->studentRepository->getById($entity->getStudentId());
        $teacher = $this->teacherRepository->getById($entity->getTeacherId());

        $entity->setStudent($student);
        $entity->setTeacher($teacher);

        $adminViewModel->setActiveEntity($entity);

        return new PageResult(self::EDIT_VIEW_TEMPLATE, $adminViewModel);
    }

    public function savePost() {
        $entityId = intval($this->getRequest()->getParam('entity_id'));

        /** @var MarkData */
        $entity = $this->getEntity($entityId);

        if(!$entity || (!$entity->getId() && $entityId)) {
            $this->messageManager->addMessage(MessageManager::MESSAGE_TYPE_ERROR, "Ocena nije pronađena");
            return $this->redirect($this->getRequest()->getRefererUrl());
        }

        $entity->setQuarter( intval($this->getRequest()->getParam('quarter')) );
        $entity->setMark( intval($this->getRequest()->getParam('mark')) );


        try {

            $this->markValidator->validateMarkEntity($entity);

            if(!$this->markRepository->save($entity)) {
                throw new \Exception();
            }
        } catch (ValidationException $e) {
            $this->messageManager->addMessage(MessageManager::MESSAGE_TYPE_ERROR, $e->getMessage());
            return $this->redirect($this->getRequest()->getRefererUrl());
        } catch (\Exception $e) {
            $this->messageManager->addMessage(MessageManager::MESSAGE_TYPE_ERROR, "Nastala je greška prilikom čuvanja ocene");
            return $this->redirect($this->getRequest()->getRefererUrl());
        }

        $this->messageManager->addMessage(MessageManager::MESSAGE_TYPE_SUCCESS, "Ocena je sačuvana");
        return $this->redirect($this->getRequest()->getRefererUrl());
    }

    public function deletePost() {
        $entityId = intval($this->getRequest()->getParam('entity_id'));
        /** @var MarkData $entity */
        $entity = $this->getEntity($entityId);

        if($entity->getId() !== $entityId) {
            $this->messageManager->addMessage(MessageManager::MESSAGE_TYPE_ERROR, "Ne postoji ova ocena");
            return $this->redirect($this->getRequest()->getRefererUrl());
        }

        try {
            if(!$this->markRepository->deleteById($entityId)) {
                $this->messageManager->addMessage(MessageManager::MESSAGE_TYPE_ERROR, "Nastala je greška prilikom izmene ocene");
                return $this->redirect($this->getRequest()->getRefererUrl());
            }
            $this->messageManager->addMessage(MessageManager::MESSAGE_TYPE_SUCCESS, "Ocena je obrisana");
            return $this->redirect($this->getRequest()->getRefererUrl());
        } catch (\Exception $e) {
            throw $e;
        }
    }

    public function restorePost() {
        $entityId = intval($this->getRequest()->getParam('entity_id'));
        /** @var MarkData $entity */
        $entity = $this->getEntity($entityId);

        if($entity->getId() !== $entityId) {
            $this->messageManager->addMessage(MessageManager::MESSAGE_TYPE_ERROR, "Ne postoji ova ocena");
            return $this->redirect($this->getRequest()->getRefererUrl());
        }

        try {
            if(!$this->markRepository->restoreById($entityId)) {
                $this->messageManager->addMessage(MessageManager::MESSAGE_TYPE_ERROR, "Nastala je greška prilikom izmene ocene");
                return $this->redirect($this->getRequest()->getRefererUrl());
            }
            $this->messageManager->addMessage(MessageManager::MESSAGE_TYPE_SUCCESS, "Ocena je vraćena");
            return $this->redirect($this->getRequest()->getRefererUrl());
        } catch (\Exception $e) {
            throw $e;
        }
    }

}