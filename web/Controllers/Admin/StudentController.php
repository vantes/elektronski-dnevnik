<?php declare(strict_types = 1);

namespace App\Controllers\Admin;

use App\Services\ServiceManager;
use App\Core\DatabaseConnection;
use App\Core\Session\Session;
use App\Models\Data\DataCollection;
use App\Services\MessageManager;
use App\Services\Repository\StudentRepository;
use App\Models\Data\EntityInterface;
use App\Models\Data\StudentData;
use App\Models\Data\UserData;
use App\Models\View\AdminViewConfiguration;
use App\Services\AccountManager;

class StudentController extends AbstractController {

    const LIST_URL = "/admin/student";
    const VIEW_URL = "/admin/student/view";
    const EDIT_URL = "/admin/student/edit";
    const SAVE_URL = "/admin/student/savePost";
    const DELETE_URL = "/admin/student/deletePost";
    const RESTORE_URL = "/admin/student/restorePost";

    /** @var StudentRepository */
    private $studentRepository;

    /** @var AccountManager */
    private $accountManager;

    public function __construct(
        DatabaseConnection $dbc,
        Session $session,
        MessageManager $messageManager
    ) {
        parent::__construct($dbc, $session, $messageManager);
        $this->studentRepository = ServiceManager::getService(StudentRepository::class);
        $this->accountManager = ServiceManager::getService(AccountManager::class);
    }

    /**
     * @inheritDoc
     *
     * @return AdminViewConfiguration
     */
    protected function getViewConfiguration(): AdminViewConfiguration {
        $viewConfiguration = new AdminViewConfiguration();
        $viewConfiguration->setViewUrl(self::VIEW_URL);
        $viewConfiguration->setEditUrl(self::EDIT_URL);
        $viewConfiguration->setSaveUrl(self::SAVE_URL);
        $viewConfiguration->setDeleteUrl(self::DELETE_URL);
        $viewConfiguration->setRestoreUrl(self::RESTORE_URL);
        return $viewConfiguration;
    }

    /**
     * @inheritDoc
     *
     * @param integer|null $id
     * @return EntityInterface
     */
    public function getEntity(?int $id): EntityInterface {
        if($id) {
            return $this->studentRepository->getById($id);
        }
        $entity = new StudentData();
        $entity->setUser(new UserData());
        return $entity;
    }

    /**
     * @inheritDoc
     *
     * @param array $params
     * @return array
     */
    public function getList(array $params, bool $applyDefaultFilters = false): DataCollection {
        $items =  $this->studentRepository->search($params['filters'], $params['page'], $params['limit'], $applyDefaultFilters);
        $count =  $this->studentRepository->count($params['filters'], $applyDefaultFilters);

        $collection = new DataCollection();
        $collection->setItems($items);
        $collection->setPage($params['page']);
        $collection->setPageSize($params['limit']);
        $collection->setSize($count);

        return $collection;
    }

    /**
     * @inheritDoc
     *
     * @return void
     */
    public function savePost() {
        $entityId = intval($this->getRequest()->getParam('entity_id'));

        /** @var StudentData */
        $entity = $this->getEntity($entityId);

        if(!$entity || (!$entity->getId() && $entityId)) {
            $this->messageManager->addMessage(MessageManager::MESSAGE_TYPE_ERROR, "Učenik nije pronađen");
            return $this->redirect($this->getRequest()->getRefererUrl());
        }

        if($this->getRequest()->getParam('password')) {
            $entity->getUser()->setPasswordHash($this->accountManager->generatePasswordHash($this->getRequest()->getParam('password')));
        }

        $entity->setForename( $this->getRequest()->getParam('forename') );
        $entity->setSurname( $this->getRequest()->getParam('surname') );
        $entity->setEmail( $this->getRequest()->getParam('email') );
        $entity->setAddress( $this->getRequest()->getParam('address') );
        $entity->setPhone( $this->getRequest()->getParam('phone') );
        $entity->setGender( $this->getRequest()->getParam('gender') );
        $entity->setDivision( $this->getRequest()->getParam('division') );
        $entity->setYear( $this->getRequest()->getParam('year') );

        try {
            if(!$this->studentRepository->save($entity)) {
                throw new \Exception();
            }
        } catch (\Exception $e) {
            $this->messageManager->addMessage(MessageManager::MESSAGE_TYPE_ERROR, "Nastala je greška prilikom čuvanja podataka o učeniku");
            return $this->redirect($this->getRequest()->getRefererUrl());
        }

        $this->messageManager->addMessage(MessageManager::MESSAGE_TYPE_SUCCESS, "Učenik je sačuvan");
        return $this->redirect($this->getRequest()->getRefererUrl());
    }

    public function deletePost() {
        $entityId = intval($this->getRequest()->getParam('entity_id'));
        /** @var SubjectData $entity */
        $entity = $this->getEntity($entityId);

        if($entity->getId() !== $entityId) {
            $this->messageManager->addMessage(MessageManager::MESSAGE_TYPE_ERROR, "Ne postoji ovaj učenik");
            return $this->redirect($this->getRequest()->getRefererUrl());
        }

        try {
            if(!$this->studentRepository->deleteById($entityId)) {
                $this->messageManager->addMessage(MessageManager::MESSAGE_TYPE_ERROR, "Nastala je greška prilikom izmene podataka o učeniku");
                return $this->redirect($this->getRequest()->getRefererUrl());
            }
            $this->messageManager->addMessage(MessageManager::MESSAGE_TYPE_SUCCESS, "Učenik je obrisan");
            return $this->redirect($this->getRequest()->getRefererUrl());
        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * @inheritDoc
     *
     * @return void
     */
    public function restorePost() {
        $entityId = intval($this->getRequest()->getParam('entity_id'));
        /** @var SubjectData $entity */
        $entity = $this->getEntity($entityId);

        if($entity->getId() !== $entityId) {
            $this->messageManager->addMessage(MessageManager::MESSAGE_TYPE_ERROR, "Ne postoji ovaj učenik");
            return $this->redirect($this->getRequest()->getRefererUrl());
        }

        try {
            if(!$this->studentRepository->restoreById($entityId)) {
                $this->messageManager->addMessage(MessageManager::MESSAGE_TYPE_ERROR, "Nastala je greška prilikom izmene podataka o učeniku");
                return $this->redirect($this->getRequest()->getRefererUrl());
            }
            $this->messageManager->addMessage(MessageManager::MESSAGE_TYPE_SUCCESS, "Učenik je vraćen");
            return $this->redirect($this->getRequest()->getRefererUrl());
        } catch (\Exception $e) {
            throw $e;
        }
    }


}