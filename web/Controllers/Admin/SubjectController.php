<?php declare(strict_types = 1);

namespace App\Controllers\Admin;

use App\Services\ServiceManager;
use App\Core\DatabaseConnection;
use App\Core\Session\Session;
use App\Models\Data\DataCollection;
use App\Services\MessageManager;
use App\Services\Repository\SubjectRepository;
use App\Models\Data\EntityInterface;
use App\Models\Data\SubjectData;
use App\Models\View\AdminViewConfiguration;

class SubjectController extends AbstractController {

    const LIST_URL = "/admin/subject";
    const VIEW_URL = "/admin/subject/view";
    const EDIT_URL = "/admin/subject/edit";
    const SAVE_URL = "/admin/subject/savePost";
    const DELETE_URL = "/admin/subject/deletePost";
    const RESTORE_URL = "/admin/subject/restorePost";

    /** @var SubjectRepository */
    private $subjectRepository;

    public function __construct(
        DatabaseConnection $dbc,
        Session $session,
        MessageManager $messageManager
    ) {
        parent::__construct($dbc, $session, $messageManager);
        $this->subjectRepository = ServiceManager::getService(SubjectRepository::class);
    }

    /**
     * @inheritDoc
     *
     * @return AdminViewConfiguration
     */
    protected function getViewConfiguration(): AdminViewConfiguration {
        $viewConfiguration = new AdminViewConfiguration();
        $viewConfiguration->setViewUrl(self::VIEW_URL);
        $viewConfiguration->setEditUrl(self::EDIT_URL);
        $viewConfiguration->setSaveUrl(self::SAVE_URL);
        $viewConfiguration->setDeleteUrl(self::DELETE_URL);
        $viewConfiguration->setRestoreUrl(self::RESTORE_URL);
        return $viewConfiguration;
    }

    /**
     * @inheritDoc
     *
     * @param integer|null $id
     * @return EntityInterface
     */
    public function getEntity(?int $id): EntityInterface {
        if($id) {
            return $this->subjectRepository->getById($id);
        }
        return new SubjectData();
    }

    /**
     * @inheritDoc
     *
     * @param array $params
     * @return array
     */
    public function getList(array $params, bool $applyDefaultFilters = false): DataCollection {
        $items =  $this->subjectRepository->search($params['filters'], $params['page'], $params['limit'], $applyDefaultFilters);
        $count =  $this->subjectRepository->count($params['filters'], $applyDefaultFilters);

        $collection = new DataCollection();
        $collection->setItems($items);
        $collection->setPage($params['page']);
        $collection->setPageSize($params['limit']);
        $collection->setSize($count);

        return $collection;
    }

    /**
     * @inheritDoc
     *
     * @return void
     */
    public function savePost() {
        $entityId = intval($this->getRequest()->getParam('entity_id'));
        /** @var SubjectData $entity */
        $entity = $this->getEntity($entityId);

        if(!$entity ||  (!$entity->getId() && $entityId)) {
            $this->messageManager->addMessage(MessageManager::MESSAGE_TYPE_ERROR, "Predmet nije pronađen");
            return $this->redirect($this->getRequest()->getRefererUrl());
        }

        $entity->setId($entityId);
        $entity->setName($this->getRequest()->getParam('name'));
        $entity->setDescription($this->getRequest()->getParam('description'));

        try {
            if(!$this->subjectRepository->save($entity)) {
                throw new \Exception();
            }
            $this->messageManager->addMessage(MessageManager::MESSAGE_TYPE_SUCCESS, "Predmet je sačuvan");
            return $this->redirect(self::LIST_URL);
        } catch (\Exception $e) {
            $this->messageManager->addMessage(MessageManager::MESSAGE_TYPE_ERROR, "Nastala je greška prilikom čuvanja podataka o predmetu");
            return $this->redirect($this->getRequest()->getRefererUrl());
        }
    }

    /**
     * @inheritDoc
     */
    public function deletePost() {
        $entityId = intval($this->getRequest()->getParam('entity_id'));
        /** @var SubjectData $entity */
        $entity = $this->getEntity($entityId);

        if($entity->getId() !== $entityId) {
            $this->messageManager->addMessage(MessageManager::MESSAGE_TYPE_ERROR, "Ne postoji ovaj predmet");
            return $this->redirect($this->getRequest()->getRefererUrl());
        }

        try {
            if(!$this->subjectRepository->deleteById($entityId)) {
                $this->messageManager->addMessage(MessageManager::MESSAGE_TYPE_ERROR, "Nastala je greška prilikom izmene podataka o predmetu");
                return $this->redirect($this->getRequest()->getRefererUrl());
            }
            $this->messageManager->addMessage(MessageManager::MESSAGE_TYPE_SUCCESS, "Predmet je obrisan");
            return $this->redirect($this->getRequest()->getRefererUrl());
        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * @inheritDoc
     *
     * @return void
     */
    public function restorePost() {
        $entityId = intval($this->getRequest()->getParam('entity_id'));
        /** @var SubjectData $entity */
        $entity = $this->getEntity($entityId);

        if($entity->getId() !== $entityId) {
            $this->messageManager->addMessage(MessageManager::MESSAGE_TYPE_ERROR, "Ne postoji ovaj predmet");
            return $this->redirect($this->getRequest()->getRefererUrl());
        }

        try {
            if(!$this->subjectRepository->restoreById($entityId)) {
                $this->messageManager->addMessage(MessageManager::MESSAGE_TYPE_ERROR, "Nastala je greška prilikom izmene podataka o predmetu");
                return $this->redirect($this->getRequest()->getRefererUrl());
            }
            $this->messageManager->addMessage(MessageManager::MESSAGE_TYPE_SUCCESS, "Predmet je vraćen");
            return $this->redirect($this->getRequest()->getRefererUrl());
        } catch (\Exception $e) {
            throw $e;
        }
    }

}