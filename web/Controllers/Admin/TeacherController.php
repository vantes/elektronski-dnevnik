<?php declare(strict_types = 1);

namespace App\Controllers\Admin;

use App\Services\ServiceManager;
use App\Core\DatabaseConnection;
use App\Core\Session\Session;
use App\Models\Data\DataCollection;
use App\Services\MessageManager;
use App\Services\Repository\TeacherRepository;
use App\Models\Data\EntityInterface;
use App\Models\Data\TeacherData;
use App\Models\Data\UserData;
use App\Models\View\AdminViewConfiguration;
use App\Services\AccountManager;

class TeacherController extends AbstractController {

    const LIST_URL = "/admin/teacher";
    const VIEW_URL = "/admin/teacher/view";
    const EDIT_URL = "/admin/teacher/edit";
    const SAVE_URL = "/admin/teacher/savePost";
    const DELETE_URL = "/admin/teacher/deletePost";
    const RESTORE_URL = "/admin/teacher/restorePost";

    /** @var TeacherRepository */
    private $teacherRepository;

    /** @var AccountManager */
    private $accountManager;

    public function __construct(
        DatabaseConnection $dbc,
        Session $session,
        MessageManager $messageManager
    ) {
        parent::__construct($dbc, $session, $messageManager);
        $this->teacherRepository = ServiceManager::getService(TeacherRepository::class);
        $this->accountManager = ServiceManager::getService(AccountManager::class);
    }

    /**
     * @inheritDoc
     *
     * @return AdminViewConfiguration
     */
    protected function getViewConfiguration(): AdminViewConfiguration {
        $viewConfiguration = new AdminViewConfiguration();
        $viewConfiguration->setViewUrl(self::VIEW_URL);
        $viewConfiguration->setEditUrl(self::EDIT_URL);
        $viewConfiguration->setSaveUrl(self::SAVE_URL);
        $viewConfiguration->setDeleteUrl(self::DELETE_URL);
        $viewConfiguration->setRestoreUrl(self::RESTORE_URL);
        return $viewConfiguration;
    }

    /**
     * @inheritDoc
     *
     * @param integer|null $id
     * @return EntityInterface
     */
    public function getEntity(?int $id): EntityInterface {
        if($id) {
            return $this->teacherRepository->getById($id);
        }
        $entity = new TeacherData();
        $entity->setUser(new UserData());
        return $entity;
    }

    /**
     * @inheritDoc
     *
     * @param array $params
     * @return array
     */
    public function getList(array $params, bool $applyDefaultFilters = false): DataCollection {
        $items =  $this->teacherRepository->search($params['filters'], $params['page'], $params['limit'], $applyDefaultFilters);
        $count =  $this->teacherRepository->count($params['filters'], $applyDefaultFilters);

        $collection = new DataCollection();
        $collection->setItems($items);
        $collection->setPage($params['page']);
        $collection->setPageSize($params['limit']);
        $collection->setSize($count);

        return $collection;
    }

    /**
     * @inheritDoc
     */
    public function savePost() {
        $entityId = intval($this->getRequest()->getParam('entity_id'));

        /** @var TeacherData */
        $entity = $this->getEntity($entityId);

        if(!$entity ||  (!$entity->getId() && $entityId)) {
            $this->messageManager->addMessage(MessageManager::MESSAGE_TYPE_ERROR, "Nastavnik ne postoji");
            return $this->redirect($this->getRequest()->getRefererUrl());
        }

        if($this->getRequest()->getParam('password')) {
            $entity->getUser()->setPasswordHash($this->accountManager->generatePasswordHash($this->getRequest()->getParam('password')));
        }

        $entity->setForename( $this->getRequest()->getParam('forename') );
        $entity->setSurname( $this->getRequest()->getParam('surname') );
        $entity->setEmail( $this->getRequest()->getParam('email') );
        $entity->setGender( $this->getRequest()->getParam('gender') );
        $entity->setQualifications( $this->getRequest()->getParam('qualifications') );
        $entity->setSubjectIds( $this->getRequest()->getParam('subjects') ?: []);

        try {
            if(!$this->teacherRepository->save($entity)) {
                throw new \Exception();
            }
        } catch (\Exception $e) {
            $this->messageManager->addMessage(MessageManager::MESSAGE_TYPE_ERROR, "Nastala je greška prilikom čuvanja podataka o nastavniku: " . $e->getMessage());
            return $this->redirect($this->getRequest()->getRefererUrl());
        }

        $this->messageManager->addMessage(MessageManager::MESSAGE_TYPE_SUCCESS, "Nastavnik sačuvan");
        return $this->redirect(self::LIST_URL);
    }

    /**
     * @inheritDoc
     */
    public function deletePost() {
        $entityId = intval($this->getRequest()->getParam('entity_id'));
        /** @var SubjectData $entity */
        $entity = $this->getEntity($entityId);

        if($entity->getId() !== $entityId) {
            $this->messageManager->addMessage(MessageManager::MESSAGE_TYPE_ERROR, "Ne postoji ovaj nastavnik");
            return $this->redirect($this->getRequest()->getRefererUrl());
        }

        try {
            if(!$this->teacherRepository->deleteById($entityId)) {
                $this->messageManager->addMessage(MessageManager::MESSAGE_TYPE_ERROR, "Nastala je greška prilikom izmene podataka o nastavniku");
                return $this->redirect($this->getRequest()->getRefererUrl());
            }
            $this->messageManager->addMessage(MessageManager::MESSAGE_TYPE_SUCCESS, "Nastavnik je obrisan");
            return $this->redirect($this->getRequest()->getRefererUrl());
        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * @inheritDoc
     */
    public function restorePost() {
        $entityId = intval($this->getRequest()->getParam('entity_id'));
        /** @var SubjectData $entity */
        $entity = $this->getEntity($entityId);

        if($entity->getId() !== $entityId) {
            $this->messageManager->addMessage(MessageManager::MESSAGE_TYPE_ERROR, "Ne postoji ovaj nastavnik");
            return $this->redirect($this->getRequest()->getRefererUrl());
        }

        try {
            if(!$this->teacherRepository->restoreById($entityId)) {
                $this->messageManager->addMessage(MessageManager::MESSAGE_TYPE_ERROR, "Nastala je greška prilikom izmene podataka o nastavniku");
                return $this->redirect($this->getRequest()->getRefererUrl());
            }
            $this->messageManager->addMessage(MessageManager::MESSAGE_TYPE_SUCCESS, "Nastavnik je vraćen");
            return $this->redirect($this->getRequest()->getRefererUrl());
        } catch (\Exception $e) {
            throw $e;
        }
    }

}