<?php declare(strict_types = 1);

namespace App\Controllers\Admin;

use App\Services\ServiceManager;
use App\Core\DatabaseConnection;
use App\Core\Session\Session;
use App\Models\Data\DataCollection;
use App\Services\MessageManager;
use App\Services\Repository\UserRepository;
use App\Models\Data\EntityInterface;
use App\Models\Data\UserData;
use App\Models\View\AdminViewConfiguration;
use App\Services\AccountManager;

class UserController extends AbstractController {

    const LIST_URL = "/admin/user";
    const VIEW_URL = "/admin/user/view";
    const EDIT_URL = "/admin/user/edit";
    const SAVE_URL = "/admin/user/savePost";
    const DELETE_URL = "/admin/user/deletePost";
    const RESTORE_URL = "/admin/user/restorePost";

    /** @var UserRepository */
    private $userRepository;

    /** @var AccountManager */
    private $accountManager;

    public function __construct(
        DatabaseConnection $dbc,
        Session $session,
        MessageManager $messageManager
    ) {
        parent::__construct($dbc, $session, $messageManager);
        $this->userRepository = ServiceManager::getService(UserRepository::class);
        $this->accountManager = ServiceManager::getService(AccountManager::class);
    }

    /**
     * @inheritDoc
     *
     * @return AdminViewConfiguration
     */
    protected function getViewConfiguration(): AdminViewConfiguration {
        $viewConfiguration = new AdminViewConfiguration();
        $viewConfiguration->setViewUrl(self::VIEW_URL);
        $viewConfiguration->setEditUrl(self::EDIT_URL);
        $viewConfiguration->setSaveUrl(self::SAVE_URL);
        $viewConfiguration->setDeleteUrl(self::DELETE_URL);
        $viewConfiguration->setRestoreUrl(self::RESTORE_URL);
        return $viewConfiguration;
    }

    /**
     * @inheritDoc
     *
     * @param integer|null $id
     * @return EntityInterface
     */
    public function getEntity(?int $id): EntityInterface {
        if($id) {
            return $this->userRepository->getById($id);
        }
        return new UserData();
    }

    /**
     * @inheritDoc
     *
     * @param array $params
     * @return array
     */
    public function getList(array $params, bool $applyDefaultFilters = false): DataCollection {
        $items =  $this->userRepository->search($params['filters'], $params['page'], $params['limit'], $applyDefaultFilters);
        $count =  $this->userRepository->count($params['filters'], $applyDefaultFilters);

        $collection = new DataCollection();
        $collection->setItems($items);
        $collection->setPage($params['page']);
        $collection->setPageSize($params['limit']);
        $collection->setSize($count);

        return $collection;

    }

    /**
     * @inheritDoc
     *
     * @return void
     */
    public function savePost() {
        $entityId = intval($this->getRequest()->getParam('entity_id'));
        /** @var UserData $entity */
        $entity = $this->getEntity($entityId);

        if(!$entity ||  (!$entity->getId() && $entityId)) {
            $this->messageManager->addMessage(MessageManager::MESSAGE_TYPE_ERROR, "Korisnik nije pronađen");
            return $this->redirect($this->getRequest()->getRefererUrl());
        }

        $entity->setId($entityId);
        $entity->setEmail($this->getRequest()->getParam('email'));

        if(!$entity->getId() && !$this->getRequest()->getParam('password'))  {
            $this->messageManager->addMessage(MessageManager::MESSAGE_TYPE_ERROR, "Lozinka je obavezna za nove korisnike");
            return $this->redirect($this->getRequest()->getRefererUrl());
        }

        if($this->getRequest()->getParam('password')) {
            $passwordHash = $this->accountManager->generatePasswordHash($this->getRequest()->getParam('password'));
            $entity->setPasswordHash($passwordHash);
        }

        if (!$entity->getId()) {
            $entity->setType("admin");
        }

        try {
            if(!$this->userRepository->save($entity)) {
                throw new \Exception();
            }
            $this->messageManager->addMessage(MessageManager::MESSAGE_TYPE_SUCCESS, "Korisnik je sačuvan");
            return $this->redirect(self::LIST_URL);
        } catch (\Exception $e) {
            $this->messageManager->addMessage(MessageManager::MESSAGE_TYPE_ERROR, "Nastala je greška prilikom čuvanja podataka o korisniku");
            return $this->redirect($this->getRequest()->getRefererUrl());
        }
    }

    /**
     * @inheritDoc
     */
    public function deletePost() {
        $entityId = intval($this->getRequest()->getParam('entity_id'));
        /** @var SubjectData $entity */
        $entity = $this->getEntity($entityId);

        if($entity->getId() !== $entityId) {
            $this->messageManager->addMessage(MessageManager::MESSAGE_TYPE_ERROR, "Ne postoji ovaj korisnik");
            return $this->redirect($this->getRequest()->getRefererUrl());
        }

        try {
            if(!$this->userRepository->deleteById($entityId)) {
                $this->messageManager->addMessage(MessageManager::MESSAGE_TYPE_ERROR, "Nastala je greška prilikom izmene korisnika");
                return $this->redirect($this->getRequest()->getRefererUrl());
            }
            $this->messageManager->addMessage(MessageManager::MESSAGE_TYPE_SUCCESS, "Korisnik je obrisan");
            return $this->redirect($this->getRequest()->getRefererUrl());
        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * @inheritDoc
     */
    public function restorePost() {
        $entityId = intval($this->getRequest()->getParam('entity_id'));
        /** @var SubjectData $entity */
        $entity = $this->getEntity($entityId);

        if($entity->getId() !== $entityId) {
            $this->messageManager->addMessage(MessageManager::MESSAGE_TYPE_ERROR, "Ne postoji ovaj korisnik");
            return $this->redirect($this->getRequest()->getRefererUrl());
        }

        try {
            if(!$this->userRepository->restoreById($entityId)) {
                $this->messageManager->addMessage(MessageManager::MESSAGE_TYPE_ERROR, "Nastala je greška prilikom izmene korisnika");
                return $this->redirect($this->getRequest()->getRefererUrl());
            }
            $this->messageManager->addMessage(MessageManager::MESSAGE_TYPE_SUCCESS, "Korisnik je vraćen");
            return $this->redirect($this->getRequest()->getRefererUrl());
        } catch (\Exception $e) {
            throw $e;
        }
    }


}