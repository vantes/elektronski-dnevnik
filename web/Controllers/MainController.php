<?php declare(strict_types = 1);

namespace App\Controllers;

use App\Configuration;

use App\Core\DatabaseConnection;
use App\Core\PageResult;
use App\Core\Session\Session;
use App\Services\AccountManager;
use App\Services\MessageManager;
use App\Services\ServiceManager;

class MainController extends \App\Core\Controller
{

    const HOME_URL = '';
    const DASHBOARD_URL = '/user/dashboard/';
    const LOGIN_URL = '/user/login/';
    const ADMIN_LOGIN_URL = '/admin/login/';

    /** @var AccountManager */
    private $accountManager;

    public function __construct(
        DatabaseConnection $dbc,
        Session $session,
        MessageManager $messageManager
    ) {
        parent::__construct($dbc, $session, $messageManager);
        $this->accountManager = ServiceManager::getService(AccountManager::class);
    }

    public function home()
    {
        if($this->accountManager->isLoggedIn()) {
            return $this->redirect(Configuration::BASE . self::DASHBOARD_URL);
        }
        return new PageResult(null, null);
    }


    public function getLogin()
    {
        return new PageResult(null, null);
    }

    public function getAdminLogin()
    {
        return new PageResult("adminLogin", null);
    }

    public function postAdminLogin()
    {
        $username = $this->getRequest()->getParam('log_email');
        $password = $this->getRequest()->getParam('log_pass');

        try {
            $user = $this->accountManager->adminLogin($username, $password);
        } catch (\Exception $e) {
            $this->messageManager->addMessage(MessageManager::MESSAGE_TYPE_ERROR, $e->getMessage());
            return $this->redirect(Configuration::BASE . self::ADMIN_LOGIN_URL);
        }

        $this->redirect(Configuration::BASE . self::DASHBOARD_URL);
    }

    public function postLogin()
    {
        $username = $this->getRequest()->getParam('log_email');
        $password = $this->getRequest()->getParam('log_pass');

        try {
            $user = $this->accountManager->login($username, $password);
        } catch (\Exception $e) {
            $this->messageManager->addMessage(MessageManager::MESSAGE_TYPE_ERROR, $e->getMessage());
            return $this->redirect(Configuration::BASE . self::LOGIN_URL);
        }

        $this->redirect(Configuration::BASE . self::DASHBOARD_URL);
    }

    public function postLogout() {
        $this->accountManager->logout();
        $this->redirect(Configuration::BASE);
    }

}
