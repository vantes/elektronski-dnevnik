<?php declare(strict_types = 1);

namespace App\Controllers;

use App\Core\DatabaseConnection;
use App\Core\Session\Session;
use App\Services\Repository\UserRepository;
use App\Models\View\StudentViewModel;
use App\Services\AccountManager;
use App\Configuration;
use App\Services\ServiceManager;
use App\Core\PageResult;
use App\Models\View\TeacherViewModel;
use App\Services\MessageManager;
use App\Services\Repository\TeacherRepository;
use App\Validators\MarkValidator;
use App\Exceptions\SessionException;
use App\Exceptions\ValidationException;

class TeacherController extends \App\Core\Controller
{

    /** @var AccountManager */
    private $accountManager;

    /** @var TeacherRepository */
    private $teacherRepository;

    public function __construct(
        DatabaseConnection $dbc,
        Session $session,
        MessageManager $messageManager
    ) {
        parent::__construct($dbc, $session, $messageManager);
        $this->accountManager = ServiceManager::getService(AccountManager::class);
        $this->teacherRepository = ServiceManager::getService(TeacherRepository::class);
        $this->markValidator = ServiceManager::getService(MarkValidator::class);
    }

    /**
     * @return TeacherViewModel|null
     */
    protected function getTeacherViewModel(): ?TeacherViewModel {
        $userData = $this->getSession()->get('user_data');

        if($userData->uid == null) {
            $this->redirect(Configuration::BASE);
        }

        $teacherViewModel = new TeacherViewModel();
        $teacherViewModel->loadDataModelByUserId($userData->uid);

        return $teacherViewModel;
    }

    public function studentView()
    {
        if(!$this->checkAccessByType(['teacher', 'admin'])) {
            $this->redirect(Configuration::BASE);
        }

        /** @var TeacherViewModel $teacherViewModel */
        $teacherViewModel = $this->getTeacherViewModel();

        $studentId = intval($this->getRequest()->getParam('id'));

        $studentViewModel = new StudentViewModel();
        $studentViewModel->loadDataModel($studentId);

        if(
            !$studentViewModel ||
            !$studentViewModel->getDataModel()
        ) {
            $this->redirect(Configuration::BASE);
        }

        $teacherViewModel->setStudentViewModel($studentViewModel);

        if($teacherViewModel) {
            return new PageResult(null, $teacherViewModel);
        }

        $this->redirect(Configuration::BASE);
    }

    public function finalMarkCreateView() {
        if(!$this->checkAccessByType(['teacher', 'admin'])) {
            $this->redirect(Configuration::BASE);
        }

        /** @var TeacherViewModel $teacherViewModel */
        $teacherViewModel = $this->getTeacherViewModel();

        $studentId = intval($this->getRequest()->getParam('studentId'));
        $subjectId = intval($this->getRequest()->getParam('subjectId'));

        $studentViewModel = new StudentViewModel();
        $studentViewModel->loadDataModel($studentId);

        $subjects = $this->teacherRepository->getAssignedSubjects($teacherViewModel->getTeacher()->getId(), $subjectId);
        $subject = array_shift($subjects);

        if(
            !$subject ||
            !$studentViewModel ||
            !$studentViewModel->getDataModel() ||
            !$teacherViewModel->checkIfStudentAssigned($studentId, $subjectId)
        ) {
            $this->redirect(Configuration::BASE);
        }

        $teacherViewModel->setStudentViewModel($studentViewModel);
        $teacherViewModel->setSubject($subject);

        return new PageResult(null, $teacherViewModel);
    }

    public function finalMarkListView() {
        /** @var TeacherViewModel $teacherViewModel */
        $teacherViewModel = $this->getTeacherViewModel();

        if($teacherViewModel) {
            return new PageResult(null, $teacherViewModel);
        }

        $this->redirect(Configuration::BASE);
    }

    public function finalMarkCreatePost() {
        if(!$this->checkAccessByType(['teacher', 'admin'])) {
            $this->redirect(Configuration::BASE);
        }

        /** @var TeacherViewModel $teacherViewModel */
        $teacherViewModel = $this->getTeacherViewModel();
        if(!$teacherViewModel) {
            return $this->redirect(Configuration::BASE);
        }

        $studentId = intval($this->getRequest()->getParam('student_id'));
        $subjectId = intval($this->getRequest()->getParam('subject_id'));
        $mark = intval($this->getRequest()->getParam('mark'));
        $reason = $this->getRequest()->getParam('reason');

        $studentViewModel = new StudentViewModel();
        $studentViewModel->loadDataModel($studentId);

        $subjects = $this->teacherRepository->getAssignedSubjects($teacherViewModel->getTeacher()->getId(), $subjectId);
        $subject = array_shift($subjects);

        $teacherViewModel->setStudentViewModel($studentViewModel);
        $teacherViewModel->setSubject($subject);

        try {
            $this->markValidator->validateFinalMark(
                $teacherViewModel,
                $studentViewModel,
                $subjectId,
                $mark
            );

            $suggestedFinalMark = $teacherViewModel->getSuggestedFinalMark(true);

            $result = $this->teacherRepository->createFinalMark(
                $teacherViewModel->getDataModel()->getId(),
                $studentId,
                $subjectId,
                $mark,
                $suggestedFinalMark,
                $reason
            );

            if($result) {
                // add success message
                $this->messageManager->addMessage(MessageManager::MESSAGE_TYPE_SUCCESS, 'Uspešno zaključena ocena');
            } else {
                // add error message
                $this->messageManager->addMessage(MessageManager::MESSAGE_TYPE_ERROR, 'Nastala je greška prilikom zaključivanja ocene');
            }
        } catch (ValidationException $e) {
            // add error message
            $this->messageManager->addMessage(MessageManager::MESSAGE_TYPE_ERROR, $e->getMessage());
        } catch (\Exception $e) {
            $this->messageManager->addMessage(MessageManager::MESSAGE_TYPE_ERROR, 'Nastala je greška prilikom zaključivanja ocene: ' . $e->getMessage());
        }

        $this->redirect(Configuration::BASE . '/marks/final/create?studentId=' . $studentId . '&subjectId=' . $subjectId);
    }

    public function markCreatePost() {
        if(!$this->checkAccessByType(['teacher', 'admin'])) {
            $this->redirect(Configuration::BASE);
        }

        /** @var TeacherViewModel $teacherViewModel */
        $teacherViewModel = $this->getTeacherViewModel();
        if(!$teacherViewModel) {
            return $this->redirect(Configuration::BASE);
        }

        $studentId = intval($this->getRequest()->getParam('student_id'));
        $subjectId = intval($this->getRequest()->getParam('subject_id'));
        $mark = intval($this->getRequest()->getParam('mark'));

        $studentViewModel = new StudentViewModel();
        $studentViewModel->loadDataModel($studentId);

        $subjects = $this->teacherRepository->getAssignedSubjects($subjectId);
        $subject = array_shift($subjects);

        $teacherViewModel->setStudentViewModel($studentViewModel);
        $teacherViewModel->setSubject($subject);

        try {
            $this->markValidator->validateMark(
                $teacherViewModel,
                $studentViewModel,
                $subjectId,
                $mark
            );

            $result = $this->teacherRepository->createMark(
                $teacherViewModel->getDataModel()->getId(),
                $studentId,
                $subjectId,
                $mark
            );

            if($result) {
                // add success message
                $this->messageManager->addMessage(MessageManager::MESSAGE_TYPE_SUCCESS, 'Uspešno dodata ocena');
            } else {
                // add error message
                $this->messageManager->addMessage(MessageManager::MESSAGE_TYPE_ERROR, 'Nastala je greška prilikom dodavanja ocene');
            }
        } catch (ValidationException $e) {
            $this->messageManager->addMessage(MessageManager::MESSAGE_TYPE_ERROR, $e->getMessage());
        } catch (\Exception $e) {
            $this->messageManager->addMessage(MessageManager::MESSAGE_TYPE_ERROR, 'Nastala je greška prilikom dodavanja ocene');
        }



        $this->redirect(Configuration::BASE . '/student/view/?id=' . $studentId);
    }

}
