<?php declare(strict_types = 1);

namespace App\Controllers;

use App\Core\DatabaseConnection;
use App\Core\Session\Session;
use App\Services\Repository\UserRepository;
use App\Models\View\StudentViewModel;
use App\Models\View\TeacherViewModel;
use App\Models\View\AdminViewModel;
use App\Services\AccountManager;
use App\Configuration;
use App\Services\ServiceManager;
use App\Core\PageResult;
use App\Exceptions\SessionException;
use App\Models\View\AdminViewConfiguration;
use App\Services\MessageManager;

class UserDashboardController extends \App\Core\Controller
{

    const STUDENT_DASHBOARD_TEMPLATE = "student_dashboard";
    const TEACHER_DASHBOARD_TEMPLATE = "teacher_dashboard";
    const ADMIN_DASHBOARD_TEMPLATE = "admin_dashboard";

    /** @var AccountManager */
    private $accountManager;

    public function __construct(
        DatabaseConnection $dbc,
        Session $session,
        MessageManager $messageManager
    ) {
        parent::__construct($dbc, $session, $messageManager);
        $this->accountManager = ServiceManager::getService(AccountManager::class);
    }

    protected function showStudentDashboard($userData) {
        $studentViewModel = new StudentViewModel();
        $studentViewModel->loadDataModelByUserId($userData->uid);
        return $studentViewModel;
    }

    protected function showTeacherDashboard($userData) {
        $teacherViewModel = new TeacherViewModel();
        $teacherViewModel->loadDataModelByUserId($userData->uid);
        return $teacherViewModel;
    }

    protected function showAdminDashboard($userData) {
        $adminViewModel = new AdminViewModel(new AdminViewConfiguration());
        $adminViewModel->loadDataModel($userData->uid);
        return $adminViewModel;
    }

    public function dashboardView()
    {
        $userData = $this->getSession()->get('user_data');

        if($userData->uid == null) {
            $this->redirect(Configuration::BASE);
        }

        $userType = $userData->user_type;

        if($userType == UserRepository::USER_TYPE_TEACHER) {
            return new PageResult(self::TEACHER_DASHBOARD_TEMPLATE, $this->showTeacherDashboard($userData));
        }

        if($userType == UserRepository::USER_TYPE_STUDENT) {
            return new PageResult(self::STUDENT_DASHBOARD_TEMPLATE, $this->showStudentDashboard($userData));
        }

        if($userType == UserRepository::USER_TYPE_ADMIN) {
            return new PageResult(self::ADMIN_DASHBOARD_TEMPLATE, $this->showAdminDashboard($userData));
        }

        $this->accountManager->logout();
        $this->redirect(Configuration::BASE);
    }


}
