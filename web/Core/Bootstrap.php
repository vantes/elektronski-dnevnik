<?php declare(strict_types = 1);

namespace App\Core;

use ReflectionClass;

use App\Configuration;
use App\Core\Controller;
use App\Core\PageResult;
use App\Core\DatabaseConnection;
use App\Core\Session\Session;
use App\Core\DatabaseConfiguration;
use App\Core\Route;
use App\Core\Router;
use App\Core\Session\FileSessionStorage;
use App\Exceptions\SessionException;
use App\Services\MessageManager;
use App\Services\ServiceManager;

// for formatting dates
use Twig\Extra\Intl\IntlExtension;

class Bootstrap
{

    const RUN_MODE_DEVELOPMENT = "development";
    const RUN_MODE_PRODUCTION = "production";

    /** @var string */
    private $mode = self::RUN_MODE_PRODUCTION;

    /**
     * Runs the application
     *
     * @return void
     */
    public function run($mode = self::RUN_MODE_PRODUCTION)
    {
        $this->setRunMode($mode);

        $url = strval(filter_input(INPUT_SERVER, 'REQUEST_URI'));
        $httpMethod = filter_input(INPUT_SERVER, 'REQUEST_METHOD');

        $this->initDatabaseConnection();
        $this->initSession();
        $router = $this->initRouter();
        $route = $router->find($httpMethod, $url);

        $result = $this->executeRoute($router, $httpMethod, $url);
        $this->renderResult($route, $result);
    }

    /**
     * Sets application run mode
     *
     * @param string $mode
     * @return self
     */
    protected function setRunMode(string $mode) {
        $this->mode = $mode;
        if($this->mode == self::RUN_MODE_PRODUCTION) {
            ini_set('display_errors', '1');
        }
        return $this;
    }

    /**
     * Initializes the global database connection
     *
     * @return void
     */
    protected function initDatabaseConnection() {
        $databaseConfiguration = new DatabaseConfiguration(
            Configuration::DATABASE_HOST,
            Configuration::DATABASE_NAME,
            Configuration::DATABASE_USER,
            Configuration::DATABASE_PASS
        );
        $databaseConnection = new DatabaseConnection($databaseConfiguration);
        ServiceManager::setService(DatabaseConnection::class, $databaseConnection);
    }

    /**
     * Initializes session and session storage
     *
     * @return void
     */
    protected function initSession() {
        $sessionStorageClassName = Configuration::SESSION_STORAGE;
        $sessionStorageConstructorArguments = Configuration::SESSION_STORAGE_DATA;
        $sessionStorage = new $sessionStorageClassName(...$sessionStorageConstructorArguments);
        $session = new Session($sessionStorage, Configuration::SESSION_LIFETIME);

        ServiceManager::setService(Session::class, $session);
        ServiceManager::setService(FileSessionStorage::class, $sessionStorage);
    }

    /**
     * Initializes all routes
     *
     * @return Router
     */
    protected function initRouter(): Router {
        $router = new Router();
        $routes = require_once('Core/Routes.php');

        foreach ($routes as $route) {
            $router->add($route);
        }

        return $router;
    }

    /**
     * Executes a route based on request method and URL
     *
     * @param Router $router
     * @param string $httpMethod
     * @param string $url
     * @return PageResult
     */
    protected function executeRoute(
        Router $router,
        string $httpMethod,
        string $url
    ): PageResult {
        /** @var DatabaseConnection $dbConnection */
        $dbConnection = ServiceManager::getService(DatabaseConnection::class);
        /** @var Session $session */
        $session = ServiceManager::getService(Session::class);
        /** @var MessageManager $messageManager */
        $messageManager = ServiceManager::getService(MessageManager::class);

        /** @var Route $route */
        $route = $router->find($httpMethod, $url);
        /** @var array $arguments */
        $arguments = $route->extractArguments($url);


        $fullControllerName = '\\App\\Controllers\\' . $route->getControllerName() . "Controller";
        $controllerClass = new ReflectionClass($fullControllerName);
        /** @var \App\Core\Controller $controller */
        // equivalent to: new $fullControllerName($dbConnection, $session, $messageManager);
        $controller = $controllerClass->newInstanceArgs([
            $dbConnection,
            $session,
            $messageManager
        ]);

        try {
            $controller->getSession()->reload();

            $messageManager->loadMessages();

            $controller->__pre();

            if (!method_exists($controller, $route->getMethodName())) {
                throw new \Exception("No such method exists for route: " . $route->getMethodName());
            }

            /** @var PageResult */
            $pageResult = call_user_func_array([$controller, $route->getMethodName()], $arguments);
            $controller->getSession()->save();
        } catch (SessionException $e) {
            $controller->redirect(Configuration::BASE);
        }
        return $pageResult;
    }

    /**
     * Renders the result based on the Route and PageResult
     *
     * @param Route $route
     * @param PageResult $result
     * @return void
     */
    protected function renderResult(Route $route, PageResult $result) {
        $loader = new \Twig\Loader\FilesystemLoader('./views');
        $twig = new \Twig\Environment($loader, [
            "cache" => "./twig-cache",
            "auto_reload" => ($this->mode == self::RUN_MODE_PRODUCTION)
        ]);

        $twig->addExtension(new IntlExtension());

        $viewTemplate = $route->getControllerName() . '/' . ($result->getTemplate() ?: $route->getMethodName()) . '.html';

        /** @var MessageManager $messageManager */
        $messageManager = ServiceManager::getService(MessageManager::class);

        $html = $twig->render(
            $viewTemplate,
            [
                'messages' => $messageManager->getMessages(),
                'model' => $result->getViewModel()
            ]
        );

        echo $html;
    }
}
