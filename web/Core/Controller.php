<?php declare(strict_types = 1);

namespace App\Core;

use App\Core\DatabaseConnection;
use App\Core\Session\Session;
use App\Services\MessageManager;
use App\Core\RequestAwareTrait;

class Controller {

    use RequestAwareTrait;

    /** @var DatabaseConnection  */
    private $dbc;

    /** @var Session */
    private $session;

    /** @var MessageManager */
    protected $messageManager;

    /** @var mixed[] */
    private $data = [];


    public function __pre() {}

    /**
     *
     * @param \App\Core\DatabaseConnection $dbc
     */
    public function __construct(
        DatabaseConnection $dbc,
        Session $session,
        MessageManager $messageManager
    ) {
        $this->dbc = $dbc;
        $this->session = $session;
        $this->messageManager = $messageManager;
    }

    final public function getSession(): Session {
        return $this->session;
    }

    final public function setSession(Session $session) {
        $this->session = $session;
    }

    final public function getDatabaseConnection(): DatabaseConnection {
        return $this->dbc;
    }

    final protected function set(string $name, $value) {
        if(preg_match('/^[a-z][a-z0-9]+(?:[A-Z][a-z0-9]+)*$/', $name)) {
            $this->data[$name] = $value;
        }
    }

    final public function getData(): array {
        return $this->data;
    }

    final public function redirect(string $path, int $code = 303) {
        ob_clean();
        header('SetCookie: APPSESSION=' . $this->getSession()->getSessionId() . '; expires= ' . date(\DateTime::COOKIE, time() + 3600) . '; Max-Age=3600; path=/' , true, $code);
        header('Location: ' . $path, true, $code);
        exit;
    }

    /**
     * Compares whether the user can access the requested route
     *
     * @param string $accessRule
     * @return boolean
     */
    protected function checkAccessByType(array $allowedUserTypes): bool {
        $userData = $this->getSession()->get('user_data');
        $userType = $userData->user_type;

        return in_array($userType, $allowedUserTypes);
    }
}