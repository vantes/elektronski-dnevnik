<?php declare(strict_types = 1);

namespace App\Core;

class DatabaseConfiguration {

    /** @var string */
    private $host;

    /** @var string */
    private $user;

    /** @var string */
    private $pass;

    /** @var string */
    private $name;

    /**
     *
     * @param string $host
     * @param string $name
     * @param string $user
     * @param string $pass
     */
    public function __construct(
        string $host,
        string $name,
        string $user,
        string $pass
    ) {
        $this->host = $host;
        $this->name = $name;
        $this->user = $user;
        $this->pass = $pass;
    }

    /**
     *
     * @return string
     */
    public function getSourceString(): string {
        return "mysql:host={$this->host};port=3306;dbname={$this->name};charset=utf8;";
    }

    /**
     *
     * @return string
     */
    public function getUser(): string {
        return $this->user;
    }

    /**
     *
     * @return string
     */
    public function getPass(): string {
        return $this->pass;
    }
}
