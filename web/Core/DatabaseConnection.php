<?php declare(strict_types = 1);

namespace App\Core;

use PDO;

class DatabaseConnection
{
    /** @var \PDO|null */
    private $connection;

    /** @var DatabaseConfiguration */
    private $configuration;

    public function __construct(
        DatabaseConfiguration $databaseConfiguration
    ) {
        $this->configuration = $databaseConfiguration;
    }

    public function getConnection(): \PDO
    {
        if ($this->connection === null) {
            $this->connection = new \PDO(
                $this->configuration->getSourceString(),
                $this->configuration->getUser(),
                $this->configuration->getPass()
            );
        }
        return $this->connection;
    }
}
