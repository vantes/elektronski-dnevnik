<?php declare(strict_types = 1);

namespace App\Core;

use App\Models\View\ViewModelInterface;

class PageResult {

    /** @var string */
    private $template;

    /** @var ViewModelInterface */
    private $viewModel;

    /**
     *
     * @param string|null $template
     * @param ViewModelInterface|null $viewModel
     */
    public function __construct(
        ?string $template,
        ?ViewModelInterface $viewModel
    ) {
      $this->template = $template;
      $this->viewModel = $viewModel;
    }

    public function getTemplate() {
        return $this->template;
    }

    public function getViewModel() {
        return $this->viewModel;
    }

}