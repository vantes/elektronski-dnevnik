<?php declare(strict_types = 1);

namespace App\Core;

use App\Models\Http\RequestInterface;
use App\Services\RequestFactory;

trait RequestAwareTrait {

    /** @var RequestInterface */
    private $request;

    public function getRequest(): RequestInterface {
        if(!$this->request) {
            $this->request = RequestFactory::createRequest();
        }

        return $this->request;
    }

}