<?php declare(strict_types = 1);

namespace App\Core;

final class Route {

    /** @var string */
    private $requestMethod;

    /** @var string */
    private $pattern;

    /** @var string */
    private $controller;

    /** @var string */
    private $method;

    private function __construct(
        string $requestMethod,
        string $pattern,
        string $controller,
        string $method
    ) {
        $this->requestMethod = $requestMethod;
        $this->pattern = $pattern;
        $this->controller = $controller;
        $this->method = $method;
    }


    public static function get(
        string $pattern,
        string $controller,
        string $method
    ) {
        return new Route('GET', $pattern, $controller, $method);
    }

    public static function post(
        string $pattern,
        string $controller,
        string $method
    ) {
        return new Route('POST', $pattern, $controller, $method);
    }

    public static function any(
        string $pattern,
        string $controller,
        string $method
    ) {
        return new Route('GET|POST', $pattern, $controller, $method);
    }

    public function matches(
        string $method,
        string $url
    ): bool {
        if(!preg_match('/^' . $this->requestMethod . '$/', $method)) {
            return false;
        }
        return boolval(preg_match($this->pattern, $url));
    }

    public function getControllerName(): string {
        return $this->controller;
    }

    public function getMethodName(): string {
        return $this->method;
    }


    public function extractArguments(string $url): array {
        $arguments = [];
        preg_match_all($this->pattern, $url, $arguments);
        return $arguments;
    }
}