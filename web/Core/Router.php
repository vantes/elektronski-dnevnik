<?php declare(strict_types = 1);

namespace App\Core;

use App\Core\Route;

final class Router {

    /** @var Route[] */
    private $routes = [];

    public function __construct() { }

    /**
     *
     * @param Route $route
     * @return void
     */
    public function add(Route $route) {
        $this->routes[] = $route;
        return $this;
    }

    /**
     *
     * @param string $method
     * @param string $url
     * @return Route
     */
    public function find(string $method, string $url): Route {
        foreach($this->routes as $route) {
            if($route->matches($method, $url)) {
                return $route;
            }
        }

        return null;
    }
}