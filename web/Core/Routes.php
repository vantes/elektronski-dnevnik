<?php declare(strict_types = 1);

use App\Core\Route;

return [
    // Standard routes
    Route::get('/^\/user\/login\/?$/', 'Main', 'getLogin'),
    Route::post('/^\/user\/login\/?$/', 'Main', 'postLogin'),
    Route::post('/^\/user\/logout\/?$/', 'Main', 'postLogout'),
    Route::get('/^\/user\/dashboard\//', 'UserDashboard', 'dashboardView'),
    Route::post('/^\/user\/dashboard\/?$/', 'UserDashboard', 'dashboardView'),
    Route::get('/^\/student\/view/', 'Teacher', 'studentView'),
    Route::get('/^\/marks\/final?$/', 'Teacher', 'finalMarkListView'),
    Route::get('/^\/marks\/final\/create/', 'Teacher', 'finalMarkCreateView'),
    Route::post('/^\/marks\/final\/createPost?$/', 'Teacher', 'finalMarkCreatePost'),
    Route::get('/^\/marks\/create/', 'Teacher', 'markCreateView'),
    Route::post('/^\/marks\/createPost/', 'Teacher', 'markCreatePost'),
    // Admin routes
    // Users
    Route::get('/^\/admin\/login\/?$/', 'Main', 'getAdminLogin'),
    Route::post('/^\/admin\/login\/?$/', 'Main', 'postAdminLogin'),
    // Users
    Route::get('/^\/admin\/user\/create/', 'Admin\User', 'createView'),
    Route::get('/^\/admin\/user\/edit/', 'Admin\User', 'editView'),
    Route::post('/^\/admin\/user\/savePost/', 'Admin\User', 'savePost'),
    Route::post('/^\/admin\/user\/deletePost/', 'Admin\User', 'deletePost'),
    Route::post('/^\/admin\/user\/restorePost/', 'Admin\User', 'restorePost'),
    Route::get('/^\/admin\/user/', 'Admin\User', 'listView'),
    // Student
    Route::get('/^\/admin\/student\/create/', 'Admin\Student', 'createView'),
    Route::get('/^\/admin\/student\/edit/', 'Admin\Student', 'editView'),
    Route::post('/^\/admin\/student\/savePost/', 'Admin\Student', 'savePost'),
    Route::post('/^\/admin\/student\/deletePost/', 'Admin\Student', 'deletePost'),
    Route::post('/^\/admin\/student\/restorePost/', 'Admin\Student', 'restorePost'),
    Route::get('/^\/admin\/student/', 'Admin\Student', 'listView'),
    // Teacher
    Route::get('/^\/admin\/teacher\/create/', 'Admin\Teacher', 'createView'),
    Route::get('/^\/admin\/teacher\/edit/', 'Admin\Teacher', 'editView'),
    Route::post('/^\/admin\/teacher\/savePost/', 'Admin\Teacher', 'savePost'),
    Route::post('/^\/admin\/teacher\/deletePost/', 'Admin\Teacher', 'deletePost'),
    Route::post('/^\/admin\/teacher\/restorePost/', 'Admin\Teacher', 'restorePost'),
    Route::get('/^\/admin\/teacher/', 'Admin\Teacher', 'listView'),
    // Subject
    Route::get('/^\/admin\/subject\/create/', 'Admin\Subject', 'createView'),
    Route::get('/^\/admin\/subject\/edit/', 'Admin\Subject', 'editView'),
    Route::post('/^\/admin\/subject\/savePost/', 'Admin\Subject', 'savePost'),
    Route::post('/^\/admin\/subject\/deletePost/', 'Admin\Subject', 'deletePost'),
    Route::post('/^\/admin\/subject\/restorePost/', 'Admin\Subject', 'restorePost'),
    Route::get('/^\/admin\/subject/', 'Admin\Subject', 'listView'),
    // Mark
    Route::get('/^\/admin\/mark\/create/', 'Admin\Mark', 'createView'),
    Route::get('/^\/admin\/mark\/edit/', 'Admin\Mark', 'editView'),
    Route::post('/^\/admin\/mark\/savePost/', 'Admin\Mark', 'savePost'),
    Route::post('/^\/admin\/mark\/deletePost/', 'Admin\Mark', 'deletePost'),
    Route::post('/^\/admin\/mark\/restorePost/', 'Admin\Mark', 'restorePost'),
    Route::get('/^\/admin\/mark/', 'Admin\Mark', 'listView'),
    // Wildcard routes
    Route::any('/^.*$/', 'Main', 'home')
];