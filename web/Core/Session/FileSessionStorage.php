<?php declare(strict_types = 1);

namespace App\Core\Session;

use App\Exceptions\SessionException;

class FileSessionStorage implements SessionStorage {

    /** @var string */
    private $sessionPath;

    /**
     *
     * @param string $sessionPath
     */
    public function __construct(string $sessionPath) {
        $this->sessionPath = $sessionPath;
    }

    /**
     *
     * @param string $sessionId
     * @param string $sessionData
     * @return void
     */
    public function save(string $sessionId, string $sessionData) {
        $sessionFileName = $this->sessionPath . $sessionId .'.json';
        file_put_contents($sessionFileName, $sessionData);
    }

    /**
     *
     * @param string $sessionId
     * @return string
     */
    public function load(string $sessionId): string {
        $sessionFileName = $this->sessionPath . $sessionId .'.json';
        if(!file_exists($sessionFileName)) {
            return '{}';
        }

        $contents = file_get_contents($sessionFileName);
        // retry up to 5 times to read data
        // not sure why this is needed but sometimes the first file_get_contents fails
        while(!$contents) {
            usleep(100);
            $contents = file_get_contents($sessionFileName);
        }

        if(!$contents) {
            return '{}';
        }

        return $contents;
    }

    /**
     *
     * @param string|null $sessionId
     * @return void
     */
    public function delete(?string $sessionId) {
        $sessionFileName = $this->sessionPath . $sessionId .'.json';
        if(file_exists($sessionFileName)) {
           unlink($sessionFileName);
        }
    }

    /**
     *
     * @param integer $sessionAge
     * @return void
     */
    public function cleanup(int $sessionAge) {

    }
}