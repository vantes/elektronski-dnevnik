<?php declare(strict_types = 1);

namespace App\Core\Session;

use App\Core\Session\SessionStorage;
use App\Core\RequestAwareTrait;

final class Session {

    use RequestAwareTrait;

    /** @var SessionStorage */
    private $sessionStorage;

    /** @var mixed|null */
    private $sessionData;

    /** @var string */
    private $sessionId;

    /** @var int */
    private $sessionLife;

    public function __construct(
        SessionStorage $sessionStorage, int $sessionLife = 3600
    ) {
        $this->sessionStorage = $sessionStorage;
        $this->sessionData = (object) [];
        $this->sessionLife = $sessionLife;

        $this->sessionId = \filter_input(INPUT_COOKIE, 'APPSESSION', FILTER_SANITIZE_STRING);
        $this->sessionId = \preg_replace('|[^A-Za-z0-9]|', '', $this->sessionId);

        if(strlen($this->sessionId) !== 32) {
            $this->sessionId = $this->generateSessionId();
            unset($_COOKIE['APPSESSION']);
            setcookie('APPSESSION', $this->sessionId, time() + $this->sessionLife, "/");
        }
    }

    public function getSessionId(): string {
        return $this->sessionId;
    }

    private function generateSessionId(): string {
        $supported = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        $id = '';
        for($i = 0; $i<32; $i++) {
            $id .= $supported[rand(0, strlen($supported)-1)];
        }
        return $id;
    }

    /**
     * @deprecated
     *
     * @param string $key
     * @param mixed $value
     * @return self
     */
    public function put(string $key, $value): self {
        return $this->set($key, $value);
    }

    /**
     * Undocumented function
     *
     * @param string $key
     * @param mixed $value
     * @return self
     */
    public function set(string $key, $value): self {
        $this->sessionData->$key = $value;
        return $this;
    }

    public function get(string $key, $defaultValue = null) {
        return $this->sessionData->$key ?? $defaultValue;
    }

    public function remove(string $key) {
        if($this->exists($key)) {
            unset($this->sessionData->$key);
        }
    }

    public function clear() {
        $this->sessionData = (object) [];
    }

    public function exists(string $key): bool {
        return isset($this->sessionData->$key);
    }

    public function has(string $key): bool {
        if(!$this->exists($key)) {
            return false;
        }

        return \boolval($this->sessionData->$key);
    }

    public function save() {
        $jsonData = \json_encode($this->sessionData);
        $this->sessionStorage->save($this->sessionId, $jsonData);
        unset($_COOKIE['APPSESSION']);
        setcookie('APPSESSION', $this->sessionId, time() + $this->sessionLife, "/");
    }

    public function reload() {
        $jsonData = $this->sessionStorage->load($this->sessionId);
        $restoreData = \json_decode($jsonData);

        if(!$restoreData) {
            $this->sessionData = (object) [];
            return;
        }

        $this->sessionData = $restoreData;
    }

    public function regenerate() {
        $this->reload();

        $this->sessionStorage->delete($this->sessionId);
        $this->sessionId = $this->generateSessionId();
        $this->save();
    }
}