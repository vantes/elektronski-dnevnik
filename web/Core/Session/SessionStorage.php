<?php declare(strict_types = 1);

namespace App\Core\Session;

interface SessionStorage {

    /**
     *
     * @param string $sessionId
     * @param string $sessionData
     * @return void
     */
    public function save(string $sessionId, string $sessionData);

    /**
     *
     * @param string $sessionId
     * @return string
     */
    public function load(string $sessionId): string;

    /**
     *
     * @param string $sessionId
     * @return void
     */
    public function delete(string $sessionId);

    /**
     *
     * @param integer $sessionAge
     * @return void
     */
    public function cleanup(int $sessionAge);

}