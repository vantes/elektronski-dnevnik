<?php declare(strict_types = 1);

namespace App\Core;

interface Validator {

    /**
     *
     * @param string $value
     * @return boolean
     */
    public function isValid(string $value): bool;
}
