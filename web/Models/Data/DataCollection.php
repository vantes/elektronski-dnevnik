<?php declare(strict_types = 1);

namespace App\Models\Data;

class DataCollection {

    /**
     * Items inside the collection
     *
     * @var array
     */
    private $items = [];

    /**
     * Total size of collection
     *
     * @var integer
     */
    private $size = 0;

    /**
     * Current collection page
     *
     * @var integer
     */
    private $page = 1;

    /**
     * Collection current page size
     *
     * @var integer
     */
    private $pageSize = 5;

    public function setItems(array $items): self {
        $this->items = $items;
        return $this;
    }

    public function getItems(): array {
        return $this->items;
    }

    public function setSize(int $size): self {
        $this->size = $size;
        return $this;
    }

    public function getSize(): int {
        return $this->size;
    }

    public function setPage(int $page): self {
        $this->page = $page;
        return $this;
    }

    public function getPage(): int {
        return $this->page;
    }

    public function setPageSize(int $pageSize): self {
        $this->pageSize = $pageSize;
        return $this;
    }

    public function getPageSize(): int {
        return $this->pageSize;
    }

    public function getPageCount(): int {
        return intval( ceil($this->getSize() / $this->getPageSize()) );
    }

}