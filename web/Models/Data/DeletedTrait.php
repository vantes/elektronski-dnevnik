<?php declare(strict_types = 1);

namespace App\Models\Data;

trait DeletedTrait {

    /** @var bool */
    private $deleted;

    /**
     * Returns indication whether the entity is deleted
     */
    public function isDeleted(): bool
    {
        return (bool) $this->deleted;
    }

    /**
     * Sets the entity as deleted
     *
     * @return  self
     */
    public function setDeleted(bool $deleted): self
    {
        $this->deleted = $deleted;
        return $this;
    }

}