<?php declare(strict_types = 1);

namespace App\Models\Data;

interface EntityInterface {

    /**
     * @return int|null
     */
    public function getId(): ?int;

    /**
     *
     * @param integer $id
     * @return self
     */
    public function setId(int $id);

    /**
     * Returns column associative base array of values
     * Used for saving the entity via repository
     * Also used for converting current entity data to correct format (example: \DateTime to correct string format)
     *
     * @return array
     */
    public function getDbFields() : array;

    /**
     * Returns whether entity is deleted
     *
     * @return boolean
     */
    public function isDeleted(): bool;

    /**
     * Sets deleted flag on entity
     *
     * @param boolean $deleted
     * @return self
     */
    public function setDeleted(bool $deleted);

}