<?php declare(strict_types = 1);

namespace App\Models\Data;

trait EntityTrait {

    /** @var int */
    private $id;

    /**
     * Get the value of id
     */
    public function getId() : ?int
    {
        return $this->id;
    }

    /**
     * Set the value of id
     *
     * @return  self
     */
    public function setId(int $id)
    {
        $this->id = $id;

        return $this;
    }

}