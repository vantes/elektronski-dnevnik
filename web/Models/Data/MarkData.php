<?php declare(strict_types=1);

namespace App\Models\Data;

use App\Models\Data\EntityInterface;
use App\Models\Data\EntityTrait;

class MarkData implements EntityInterface {

    use EntityTrait;
    use DeletedTrait;

    /** @var int */
    private $studentId;

    /** @var int */
    private $subjectId;

    /** @var int */
    private $teacherId;

    /** @var int */
    private $schoolYearId;

    /** @var \DateTime */
    private $createdAt;

    /** @var int */
    private $mark;

    /** @var int */
    private $quarter;

    /** @var StudentData */
    private $student;

    /** @var TeacherData */
    private $teacher;

    /** @var SubjectData */
    private $subject;

    /**
     * @inheritDoc
     *
     * @return array
     */
    public function getDbFields(): array
    {
        return [
            'student_id' => $this->getStudentId(),
            'subject_id' => $this->getSubjectId(),
            'teacher_id' => $this->getTeacherId(),
            'school_year' => $this->getSchoolYearId(),
            'created_at' => $this->getCreatedAt()->format("Y-m-d H:i:s"),
            'mark' => $this->getMark(),
            'quarter' => $this->getQuarter(),
            'deleted' => (int) $this->isDeleted()
        ];
    }

    /**
     *
     * @return integer
     */
    public function getStudentId(): int {
        return $this->studentId;
    }

    /**
     *
     * @param integer $studentId
     * @return self
     */
    public function setStudentId(int $studentId): self {
        $this->studentId = $studentId;
        return $this;
    }

    /**
     *
     * @return integer
     */
    public function getTeacherId(): int {
        return $this->teacherId;
    }

    /**
     *
     * @param integer $teacherId
     * @return self
     */
    public function setTeacherId(int $teacherId): self {
        $this->teacherId = $teacherId;
        return $this;
    }

    /**
     *
     * @return integer
     */
    public function getSubjectId(): int {
        return $this->subjectId;
    }

    /**
     *
     * @param integer $subjectId
     * @return self
     */
    public function setSubjectId(int $subjectId): self {
        $this->subjectId = $subjectId;
        return $this;
    }

    /**
     *
     * @return integer
     */
    public function getSchoolYearId(): int {
        return $this->schoolYearId;
    }

    /**
     *
     * @param integer $schoolYearId
     * @return self
     */
    public function setSchoolYearId(int $schoolYearId): self {
        $this->schoolYearId = $schoolYearId;
        return $this;
    }

    /**
     *
     * @return \DateTime|null
     */
    public function getCreatedAt(): ?\DateTime {
        return $this->createdAt;
    }

    /**
     *
     * @param \DateTime $date
     * @return self
     */
    public function setCreatedAt(\DateTime $createdAt): self {
        $this->createdAt = $createdAt;
        return $this;
    }

    /**
     *
     * @return integer
     */
    public function getMark(): int {
        return $this->mark;
    }

    /**
     *
     * @param integer $mark
     * @return self
     */
    public function setMark(int $mark): self {
        $this->mark = $mark;
        return $this;
    }

    /**
     *
     * @return integer
     */
    public function getQuarter(): int {
        return $this->quarter;
    }

    /**
     *
     * @param integer $quarter
     * @return self
     */
    public function setQuarter(int $quarter): self {
        $this->quarter = $quarter;
        return $this;
    }

    public function setStudent(StudentData $student): self {
        $this->student = $student;
        return $this;
    }

    public function getStudent(): StudentData {
        return $this->student;
    }

    public function setTeacher(TeacherData $teacher): self {
        $this->teacher = $teacher;
        return $this;
    }

    public function getTeacher(): TeacherData {
        return $this->teacher;
    }

    public function setSubject(SubjectData $subject): self {
        $this->subject = $subject;
        return $this;
    }

    public function getSubject(): SubjectData {
        return $this->subject;
    }

}
