<?php declare(strict_types = 1);

namespace App\Models\Data;

use App\Models\Data\EntityTrait;

/**
 * Data class for resolving trimesters in the currently active school year
 */
class SchoolYear {

    use EntityTrait;
    use DeletedTrait;

    /**
     *
     * @var \DateTime
     */
    public $startOfFirstQuarter = null;

    /**
     *
     * @var \DateTime
     */
    public $endOfFirstQuarter = null;

    /**
     *
     * @var \DateTime
     */
    public $startOfSecondQuarter = null;

    /**
     *
     * @var \DateTime
     */
    public $endOfSecondQuarter = null;

    /**
     *
     * @var \DateTime
     */
    public $startOfThirdQuarter = null;

    /**
     *
     * @var \DateTime
     */
    public $endtOfThirdQuarter = null;

    /**
     *
     * @var \DateTime
     */
    public $startOfFourthQuarter = null;

    /**
     *
     * @var \DateTime
     */
    public $endOfFourthQuarter = null;

    public function getActiveTrimester($date = null) {
        if(!$date) {
            $date = date("Y-m-d H:i:s");
        }
        $time = strtotime($date);

        foreach($this->getTrimesters() as $trimester) {
            /** @var \DateTime $start */
            $start = $trimester['start'];
            /** @var \DateTime $end */
            $end = $trimester['end'];

            if($start->getTimestamp() < $time && $time < $end->getTimestamp()) {
                return $trimester;
            }
        }

        throw new \Exception("Date not in trimester range");
    }

    public function getTrimesters(): array {
        return [
            'first' => [
                'index' => 1,
                'start' => $this->startOfFirstQuarter,
                'end' => $this->endOfFirstQuarter
            ],
            'second' => [
                'index' => 2,
                'start' => $this->startOfSecondQuarter,
                'end' => $this->endOfSecondQuarter
            ],
            'third' => [
                'index' => 3,
                'start' => $this->startOfThirdQuarter,
                'end' => $this->endOfThirdQuarter
            ],
            'fourth' => [
                'index' => 4,
                'start' => $this->startOfFourthQuarter,
                'end' => $this->endOfFourthQuarter
            ],
        ];
    }

}