<?php declare(strict_types=1);

namespace App\Models\Data;

use App\Models\Data\EntityInterface;
use App\Models\Data\EntityTrait;
use App\Models\Data\UserAwareInterface;

class StudentData implements EntityInterface {

    use EntityTrait;
    use DeletedTrait;
    use UserAwareInterface;

    /** @var string */
    private $forename;

    /** @var string */
    private $surname;

    /** @var string */
    private $address;

    /** @var string */
    private $phone;

    /** @var string */
    private $division;

    /** @var string */
    private $year;

    /** @var string */
    private $gender;

    /**
     * @inheritDoc
     *
     * @return array
     */
    public function getDbFields(): array
    {
        return [
            'forename' => $this->getForename(),
            'surname' => $this->getSurname(),
            'address' => $this->getAddress(),
            'phone' => $this->getPhone(),
            'division' => $this->getDivision(),
            'year' => $this->getYear(),
            'gender' => $this->getGender(),
            'user_id' => $this->getUserId(),
            'deleted' => (int) $this->isDeleted()
        ];
    }

    public function getName(): string {
        return $this->getForename() . " " . $this->getSurname();
    }

    /**
     * Get the value of forename
     */
    public function getForename(): ?string
    {
        return $this->forename;
    }

    /**
     * Set the value of forename
     *
     * @return  self
     */
    public function setForename(string $forename)
    {
        $this->forename = $forename;

        return $this;
    }

    /**
     * Get the value of surname
     */
    public function getSurname(): ?string
    {
        return $this->surname;
    }

    /**
     * Set the value of surname
     *
     * @return  self
     */
    public function setSurname(string $surname)
    {
        $this->surname = $surname;

        return $this;
    }

    /**
     * Get the value of address
     */
    public function getAddress(): ?string
    {
        return $this->address;
    }

    /**
     * Set the value of address
     *
     * @return  self
     */
    public function setAddress(string $address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get the value of phone
     */
    public function getPhone(): ?string
    {
        return $this->phone;
    }

    /**
     * Set the value of phone
     *
     * @return  self
     */
    public function setPhone(string $phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get the value of division
     */
    public function getDivision(): ?string
    {
        return $this->division;
    }

    /**
     * Set the value of division
     *
     * @return  self
     */
    public function setDivision(string $division)
    {
        $this->division = $division;

        return $this;
    }

    /**
     * Get the value of year
     */
    public function getYear(): ?string
    {
        return $this->year;
    }

    /**
     * Set the value of year
     *
     * @return  self
     */
    public function setYear(string $year)
    {
        $this->year = $year;

        return $this;
    }

    /**
     * Get the value of gender
     */
    public function getGender(): ?string
    {
        return $this->gender;
    }

    /**
     * Set the value of gender
     *
     * @return  self
     */
    public function setGender(string $gender)
    {
        $this->gender = $gender;

        return $this;
    }
}
