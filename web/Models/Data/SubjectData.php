<?php declare(strict_types=1);

namespace App\Models\Data;

use App\Models\Data\EntityInterface;
use App\Models\Data\EntityTrait;

class SubjectData implements EntityInterface {

    use EntityTrait;
    use DeletedTrait;

    /** @var string */
    private $name;

    /** @var string */
    private $description;


    /**
     * @inheritDoc
     *
     * @return array
     */
    public function getDbFields(): array
    {
        return [
            'name' => $this->getName(),
            'description' => $this->getDescription(),
            'deleted' => (int) $this->isDeleted()
        ];
    }

    /**
     *
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     *
     * @param string $name
     * @return self
     */
    public function setName(string $name): self
    {
        $this->name = $name;
        return $this;
    }

    /**
     *
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     *
     * @return  self
     */
    public function setDescription(string $description): self
    {
        $this->description = $description;
        return $this;
    }

}
