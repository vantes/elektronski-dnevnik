<?php declare(strict_types = 1);

namespace App\Models\Data;

use App\Models\Data\EntityInterface;
use App\Models\Data\EntityTrait;
use App\Models\Data\UserAwareInterface;

class TeacherData implements EntityInterface {

    use EntityTrait;
    use DeletedTrait;
    use UserAwareInterface;

    /** @var string */
    private $forename;

    /** @var string */
    private $surname;

    /** @var string */
    private $qualifications;

    /** @var string */
    private $gender;

    /** @var array */
    private $subjectIds = [];

    /**
     * @inheritDoc
     *
     * @return array
     */
    public function getDbFields(): array
    {
        return [
            'forename' => $this->getForename(),
            'surname' => $this->getSurname(),
            'qualifications' => $this->getQualifications(),
            'gender' => $this->getGender(),
            'user_id' => $this->getUserId(),
            'deleted' => (int) $this->isDeleted()
        ];
    }

    public function getName(): string {
        return $this->getForename() . " " . $this->getSurname();
    }

    /**
     * Get the value of forename
     */
    public function getForename(): ?string
    {
        return $this->forename;
    }

    /**
     * Set the value of forename
     *
     * @return  self
     */
    public function setForename(string $forename)
    {
        $this->forename = $forename;

        return $this;
    }

    /**
     * Get the value of surname
     */
    public function getSurname(): ?string
    {
        return $this->surname;
    }

    /**
     * Set the value of surname
     *
     * @return  self
     */
    public function setSurname(string $surname)
    {
        $this->surname = $surname;

        return $this;
    }

    /**
     * Get the value of qualifications
     */
    public function getQualifications(): ?string
    {
        return $this->qualifications;
    }

    /**
     * Set the value of qualifications
     *
     * @return  self
     */
    public function setQualifications(string $qualifications)
    {
        $this->qualifications = $qualifications;

        return $this;
    }

    /**
     * Get the value of gender
     */
    public function getGender(): ?string
    {
        return $this->gender;
    }

    /**
     * Set the value of gender
     *
     * @return  self
     */
    public function setGender(string $gender)
    {
        $this->gender = $gender;

        return $this;
    }

    /**
     * Gets the ids of assigned subjects
     *
     * @return array
     */
    public function getSubjectIds() : array {
        return $this->subjectIds;
    }

    /**
     * Sets the ids of assigned subjects
     *
     * @param array $subjectIds
     * @return sels
     */
    public function setSubjectIds(array $subjectIds) {
        $this->subjectIds = $subjectIds;

        return $this;
    }
}