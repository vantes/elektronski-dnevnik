<?php namespace App\Models\Data;

use App\Models\Data\UserData;

trait UserAwareInterface {

    /** @var int */
    private $userId;

    /** @var UserData */
    private $user;

    public function getUserId(): ?int {
        return $this->userId;
    }

    public function setUserId(int $userId): self {
        $this->userId = $userId;
        return $this;
    }

    /**
     * Set related user entity
     *
     * @param UserData $user
     * @return self
     */
    public function setUser(UserData $user): self {
        $this->user = $user;
        return $this;
    }

    /**
     * Get related user entity
     *
     * @return UserData
     */
    public function getUser(): UserData {
        return $this->user;
    }

    /**
     * Proxy function for setting email
     *
     * @param string $email
     * @return self
     */
    public function setEmail(string $email): self {
        $this->getUser()->setEmail($email);
        return $this;
    }

    /**
     * Proxy function for getting email
     *
     * @return void
     */
    public function getEmail() {
        return $this->getUser()->getEmail();
    }

}