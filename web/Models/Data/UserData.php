<?php declare(strict_types = 1);

namespace App\Models\Data;
use App\Models\Data\EntityInterface;
use App\Models\Data\EntityTrait;


class UserData implements EntityInterface {

    use EntityTrait;
    use DeletedTrait;

    /** @var \DateTime */
    private $created_at;

    /** @var string */
    private $email;

    /** @var string */
    private $password_hash;

    /** @var bool */
    private $is_active;

    /** @var string */
    private $type;

    /**
     * @inheritDoc
     *
     * @return array
     */
    public function getDbFields(): array
    {
        return [
            'created_at' => $this->getCreatedAt() ? $this->getCreatedAt()->format("Y-m-d H:i:s") : (new \DateTime())->format("Y-m-d H:i:s"),
            'email' => $this->getEmail(),
            'password_hash' => $this->getPasswordHash(),
            'type' => $this->getType(),
            'deleted' => (int) $this->isDeleted()
        ];
    }


    public function getCreatedAt(): ?\DateTime {
        return $this->created_at;
    }

    public function setCreatedAt(\DateTime $date) {
        $this->created_at = $date;
        return $this;
    }

    public function getEmail(): ?string {
        return $this->email;
    }

    public function setEmail(string $email) {
        $this->email = $email;
        return $this;
    }

    public function getPasswordHash(): ?string {
        return $this->password_hash;
    }

    public function setPasswordHash(string $hash): self {
        $this->password_hash = $hash;
        return $this;
    }

    public function getIsActive(): ?bool {
        return (bool) $this->is_active;
    }

    public function setIsActive(bool $active): self {
        $this->is_active = $active;
        return $this;
    }

    public function getType(): ?string {
        return $this->type;
    }

    public function setType(string $type): self {
        $this->type = $type;
        return $this;
    }

}