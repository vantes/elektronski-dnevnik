<?php declare(strict_types = 1);

namespace App\Models\Http;

class Request implements RequestInterface {

    /** @var array */
    private $httpGetParams = [];

    /** @var array */
    private $httpPostParams = [];

    /** @var array */
    private $params = [];

    /** @var array */
    private $server = [];

    /**
     * @inheritDoc
     *
     * @return array
     */
    public function getParams(): array {
        return $this->params;
    }

    /**
     * @inheritDoc
     *
     * @param string $key
     * @param mixed|null $default
     * @return mixed|null
     */
    public function getParam(string $key, $default = null) {
        if(isset($this->httpPostParams[$key])) {
            return $this->httpPostParams[$key];
        }
        if(isset($this->httpGetParams[$key])) {
            return $this->httpGetParams[$key];
        }

        return $default;
    }

    private function recursiveSet(&$array, $key, $valueArray) {
        $array[$key] = [];
        foreach($valueArray as $value) {
            if(is_array($value)) {
                $this->recursiveSet($array, $key, $value);
            } else {
                $array[$key][] = htmlspecialchars($value);
            }
        }
    }

    /**
     * @inheritDoc
     *
     * @param string $key
     * @param mixed $value
     * @param string $type
     * @return RequestInterface
     */
    public function setParam(string $key, $value, string $type = RequestInterface::PARAM_TYPE_GET): RequestInterface {
        switch($type) {
            case RequestInterface::PARAM_TYPE_GET:
                if(is_array($value)) {
                    $this->recursiveSet($this->httpGetParams, $key, $value);
                } else {
                    $this->httpGetParams[$key] = htmlspecialchars($value);
                }
                break;
            case RequestInterface::PARAM_TYPE_POST:
                if(is_array($value)) {
                    $this->recursiveSet($this->httpPostParams, $key, $value);
                } else {
                    $this->httpPostParams[$key] = htmlspecialchars($value);
                }
                break;
        }
        $this->params[$key] = $value;

        return $this;
    }

    /**
     * @inheritDoc
     *
     * @return string|null
     */
    public function getRefererUrl(): ?string
    {
        $serverInfo = $this->getServerInfo();
        return isset($serverInfo['HTTP_REFERER']) ? $serverInfo['HTTP_REFERER'] : null;
    }

    /**
     * @inheritDoc
     *
     * @return array
     */
    public function getServerInfo() : array {
        return $this->server;
    }

    /**
     * @inheritDoc
     *
     * @param array $serverInfo
     * @return RequestInterface
     */
    public function setServerInfo(array $serverInfo): RequestInterface {
        $this->server = $serverInfo;
        return $this;
    }

}