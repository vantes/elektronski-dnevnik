<?php declare(strict_types = 1);

namespace App\Models\Http;

interface RequestInterface {

    const PARAM_TYPE_GET = "GET";
    const PARAM_TYPE_POST = "POST";

    /**
     * Returns all set request parameters
     *
     * @return array
     */
    public function getParams(): array;

    /**
     * Returns GET or POST parameter by key
     *
     * @param string $key
     * @param mixed $default
     * @return mixed|null
     */
    public function getParam(string $key, $default = null);

    /**
     * Sets a request parameter by key and type
     *
     * @param string $key
     * @param mixed $value
     * @param string $type
     * @return RequestInterface
     */
    public function setParam(string $key, $value, string $type = RequestInterface::PARAM_TYPE_GET): RequestInterface;

    /**
     * Returns referer URL
     *
     * @return string|null
     */
    public function getRefererUrl(): ?string;

    /**
     * Returns server information
     *
     * @return array
     */
    public function getServerInfo() : array;

    /**
     * Sets server information array
     *
     * @param array $serverInfo
     * @return RequestInterface
     */
    public function setServerInfo(array $serverInfo): RequestInterface;

}