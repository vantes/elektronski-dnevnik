<?php declare(strict_types = 1);

namespace App\Models;

use JsonSerializable;

class Message implements JsonSerializable {

    /** @var string */
    private $type;

    /** @var string */
    private $message;

    public function getType() {
        return $this->type;
    }

    public function setType($type) {
        $this->type = $type;
        return $this;
    }

    public function getMessage() {
        return $this->message;
    }

    public function setMessage($message) {
        $this->message = $message;
        return $this;
    }

    public function jsonSerialize() {
        return [
            'type' => $this->type,
            'message' => $this->message
        ];
    }

}