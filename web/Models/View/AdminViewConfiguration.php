<?php namespace App\Models\View;

use App\Models\Data\DataCollection;
use App\Models\Data\EntityInterface;

class AdminViewConfiguration {

    const CONFIGURATION_VIEW_URL = "view_url";
    const CONFIGURATION_EDIT_URL = "edit_url";
    const CONFIGURATION_SAVE_URL = "save_url";
    const CONFIGURATION_DELETE_URL = "delete_url";
    const CONFIGURATION_RESTORE_URL = "restore_url";

    private $configuration = [
        self::CONFIGURATION_VIEW_URL => "",
        self::CONFIGURATION_EDIT_URL => "",
        self::CONFIGURATION_SAVE_URL => "",
        self::CONFIGURATION_DELETE_URL => "",
        self::CONFIGURATION_RESTORE_URL => "",
    ];

    /**
     * Active list on list view
     *
     * @var DataCollection|null
     */
    public $activeList = null;

    /**
     * Active entity on create / edit view
     *
     * @var EntityInterface
     */
    public $activeEntity;

    public function setActiveList(DataCollection $list): self {
        $this->activeList = $list;
        return $this;
    }

    public function getActiveList(): DataCollection {
        return $this->activeList;
    }

    public function setActiveEntity(EntityInterface $entity): self {
        $this->activeEntity = $entity;
        return $this;
    }

    public function getActiveEntity(): EntityInterface {
        return $this->activeEntity;
    }

    public function getConfiguration($key) {
        return (isset($this->configuration[$key]) ? $this->configuration[$key] : null);
    }

    public function setViewUrl(string $url): self {
        $this->configuration[AdminViewConfiguration::CONFIGURATION_VIEW_URL] = $url;
        return $this;
    }

    public function setEditUrl(string $url): self {
        $this->configuration[AdminViewConfiguration::CONFIGURATION_EDIT_URL] = $url;
        return $this;
    }

    public function setSaveUrl(string $url): self {
        $this->configuration[AdminViewConfiguration::CONFIGURATION_SAVE_URL] = $url;
        return $this;
    }

    public function setDeleteUrl(string $url): self {
        $this->configuration[AdminViewConfiguration::CONFIGURATION_DELETE_URL] = $url;
        return $this;
    }

    public function setRestoreUrl(string $url): self {
        $this->configuration[AdminViewConfiguration::CONFIGURATION_RESTORE_URL] = $url;
        return $this;
    }

}