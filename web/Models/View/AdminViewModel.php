<?php declare(strict_types = 1);

namespace App\Models\View;

use App\Models\Data\DataCollection;
use App\Models\Data\EntityInterface;
use App\Models\Data\SubjectData;
use App\Models\Data\TeacherData;
use App\Models\View\ViewModelInterface;
use App\Services\Repository\UserRepository;
use App\Services\ServiceManager;
use App\Models\View\DataModelTrait;
use App\Models\View\AdminViewConfiguration;
use App\Services\Repository\SubjectRepository;
use App\Services\Repository\TeacherRepository;

class AdminViewModel implements ViewModelInterface {

    const CONFIGURATION_VIEW_URL = "viewUrl";
    const CONFIGURATION_EDIT_URL = "editUrl";
    const CONFIGURATION_DELETE_URL = "";

    use DataModelTrait;

    /** @var UserRepository */
    private $userRepository;

    /** @var TeacherRepository */
    private $teacherRepository;

    /** @var SubjectRepository */
    private $subjectRepository;

    /** @var AdminViewConfiguration */
    protected $configuration;

    public function __construct(
        AdminViewConfiguration $configuration
    ) {
        $this->userRepository = ServiceManager::getService(UserRepository::class);
        $this->teacherRepository = ServiceManager::getService(TeacherRepository::class);
        $this->subjectRepository = ServiceManager::getService(SubjectRepository::class);
        $this->configuration = $configuration;
    }

    public function loadDataModel(int $id) : ?EntityInterface {
        $this->dataModel = $this->userRepository->getById($id);
        return $this->dataModel;
    }

    public function setActiveList(DataCollection $list): self {
        $this->configuration->activeList = $list;
        return $this;
    }

    public function getActiveList(): DataCollection {
        return $this->configuration->activeList;
    }

    public function setActiveEntity(EntityInterface $entity): self {
        $this->configuration->activeEntity = $entity;
        return $this;
    }

    public function getActiveEntity(): EntityInterface {
        return $this->configuration->activeEntity;
    }

    public function getViewUrl(EntityInterface $entity): string {
        return $this->configuration->getConfiguration(AdminViewConfiguration::CONFIGURATION_VIEW_URL) . "?id=" . $entity->getId();
    }

    public function getEditUrl(EntityInterface $entity): string {
        return $this->configuration->getConfiguration(AdminViewConfiguration::CONFIGURATION_EDIT_URL) . "?id=" . $entity->getId();
    }

    public function getSaveUrl(): string {
        return $this->configuration->getConfiguration(AdminViewConfiguration::CONFIGURATION_SAVE_URL);
    }

    public function getDeleteUrl(): string {
        return $this->configuration->getConfiguration(AdminViewConfiguration::CONFIGURATION_DELETE_URL);
    }

    public function getRestoreUrl(): string {
        return $this->configuration->getConfiguration(AdminViewConfiguration::CONFIGURATION_RESTORE_URL);
    }

    public function getTeacherSubjects(TeacherData $teacher) {
        /** @var SubjectData[] */
        $subjects = $this->subjectRepository->search([]);

        $result = [];
        foreach($subjects as $subject) {
            $result[$subject->getId()] = [
                'id' => $subject->getId(),
                'name' => $subject->getName(),
                'description' => $subject->getDescription(),
                'selected' => in_array($subject->getId(), $teacher->getSubjectIds())
            ];
        }

        return $result;
    }

}