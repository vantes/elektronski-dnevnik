<?php declare(strict_types = 1);

namespace App\Models\View;

use App\Models\Data\EntityInterface;

trait DataModelTrait {

    /** @var EntityInterface */
    private $dataModel;

    public function getDataModel(): ?EntityInterface {
        return $this->dataModel;
    }

    public function setDataModel(EntityInterface $e) {
        $this->dataModel = $e;
    }

}