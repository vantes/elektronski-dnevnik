<?php declare(strict_types = 1);

namespace App\Models\View;

use App\Models\Data\EntityInterface;
use App\Models\View\ViewModelInterface;
use App\Services\Repository\UserRepository;
use App\Services\Repository\StudentRepository;
use App\Services\ServiceManager;
use App\Models\View\DataModelTrait;

class StudentViewModel implements ViewModelInterface {

    const TRIMESTER_COUNT = 4;

    use DataModelTrait;

    /** @var UserRepository */
    private $userRepository;

    /** @var StudentRepository */
    private $studentRepository;

    public function __construct() {
        $this->userRepository = ServiceManager::getService(UserRepository::class);
        $this->studentRepository = ServiceManager::getService(StudentRepository::class);
    }

    public function loadDataModelByUserId(int $userId): ?EntityInterface {
        $this->dataModel = array_shift($this->studentRepository->search(['user_id' => $userId]));
        if(!$this->dataModel) {
            throw new \Exception("Ne postoji ovaj entitet: " . $userId);
        }
        return $this->dataModel;
    }

    public function loadDataModel(int $id) : ?EntityInterface {
        $this->dataModel = $this->studentRepository->getById($id);
        if(!$this->dataModel) {
            throw new \Exception("Ne postoji ovaj entitet");
        }
        return $this->dataModel;
    }

    public function getStudent() {
        return $this->getDataModel();
    }

    public function getSubjects() {
        return $this->studentRepository->getStudentSubjects($this->getDataModel()->getId());
    }

    public function getSubjectsData() {
        $subjectsData = [];
        foreach($this->getStudentListensData() as $subjectObj) {
            $subjectId = intval($subjectObj->subject_id);
            $subjectsData[$subjectId] = [
                'subject_id' => $subjectId,
                'subject_name' => $subjectObj->name,
                'trimesters' => []
            ];
            for ($i = 1; $i <= self::TRIMESTER_COUNT; $i++) {
                $trimesterGrades = $this->studentRepository->getTrimesterGrades($this->getDataModel()->getId(), $subjectId, $i);
                $averageTrimesterGrade = $this->studentRepository->getTrimesterAverageGrade($this->getDataModel()->getId(), $subjectId, $i);
                $subjectsData[$subjectId]['trimesters'][$i] = [
                    'grades' => is_array($trimesterGrades) ? $trimesterGrades : [],
                    'average_grade' => $averageTrimesterGrade
                ];
            }
        }

        return $subjectsData;
    }

    public function getSubjectTrimesterGrades($subjectsData, int $trimester) {
        if(isset($subjectsData['trimesters'][$trimester]['grades'])) {
            return $subjectsData['trimesters'][$trimester]['grades'];
        }
        return [];
    }

    public function getSubjectAverageGrade($subjectsData, int $trimester) {
        if(isset($subjectsData['trimesters'][$trimester]['average_grade'])) {
            return $subjectsData['trimesters'][$trimester]['average_grade'];
        }
        return null;
    }

    public function getStudentListensData() {
        return $this->studentRepository->getStudentSubjects(
            $this->getDataModel()->getId()
        );
    }

    public function getFinalMark($studentId, $subjectId) {
        return $this->studentRepository->getFinalMark(
            intval($studentId),
            intval($subjectId)
        );
    }

}