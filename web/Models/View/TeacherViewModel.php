<?php declare(strict_types = 1);

namespace App\Models\View;

use App\Models\Data\EntityInterface;
use App\Models\Data\StudentData;
use App\Models\View\ViewModelInterface;
use App\Services\Repository\UserRepository;
use App\Services\ServiceManager;
use App\Models\View\DataModelTrait;
use App\Services\Repository\StudentRepository;
use App\Services\Repository\TeacherRepository;
use App\Core\RequestAwareTrait;

class TeacherViewModel implements ViewModelInterface {

    use DataModelTrait;
    use RequestAwareTrait;

    /** @var UserRepository */
    private $userRepository;

    /** @var TeacherRepository */
    private $teacherRepository;

    /** @var StudentRepository */
    private $studentRepository;

    /** @var SubjectRepository */
    private $subjectRepository;

    public function __construct() {
        $this->userRepository = ServiceManager::getService(UserRepository::class);
        $this->teacherRepository = ServiceManager::getService(TeacherRepository::class);
        $this->studentRepository = ServiceManager::getService(StudentRepository::class);
    }

    public function loadDataModelByUserId(int $userId): ?EntityInterface {
        $this->dataModel = array_shift($this->teacherRepository->search(['user_id' => $userId]));
        if(!$this->dataModel) {
            throw new \Exception("No such entity exists");
        }
        return $this->dataModel;
    }

    public function loadDataModel(int $id) : ?EntityInterface {
        $this->dataModel = $this->teacherRepository->getById($id);
        if(!$this->dataModel) {
            throw new \Exception("No such entity exists");
        }
        return $this->dataModel;
    }

    public function getTeacher() {
        return $this->getDataModel();
    }

    public function getAssignedSubjects() {
        return $this->teacherRepository->getAssignedSubjects(
            $this->getDataModel()->getId()
        );
    }

    public function getSearchRequest() {
        if (!$this->getRequest()->getParam('search_request')) {
            return null;
        }

        $post = $this->getRequest()->getParams();
        $searchRequest = $post['search_request'];

        return $searchRequest;
    }

    /**
     * @return aray
     */
    public function getSearchResults(): array {
        $searchRequest = $this->getSearchRequest();
        if(!$searchRequest) {
            return [];
        }


        $students = $this->studentRepository->search([
            'email' => $searchRequest,
            'type'  => 'student'
        ]);
        return $students;
    }

    public function checkIfStudentAssigned(int $studentId, int $subjectId): bool {
        return $this->teacherRepository->checkIfStudentListens(
            $this->getDataModel()->getId(),
            $studentId,
            $subjectId
        );
    }

    public function getAverageSubjectMarks($subjectId): array {
        $result = [];
        for($i = 1; $i <= 4; $i ++) {
            $averageMark = $this->studentRepository->getTrimesterAverageGrade(
                $this->getStudentViewModel()->getDataModel()->getId(),
                intval($subjectId),
                $i
            );
            $result[] = ['quarter' => $i, 'average_mark' => round($averageMark, 2)];
        }

        return $result;
    }

    public function getSubjectMarks($subjectId): array {
        return $this->studentRepository->getMarks(
            $this->getStudentViewModel()->getDataModel()->getId(),
            intval($subjectId)
        );
    }

    public function getStudentSubjects(): array {
        return $this->studentRepository->getStudentSubjects(
            $this->studentViewModel->getDataModel()->getId(),
            $this->getDataModel()->getId()
        );
    }

    public function getStudentsWithFinalMarks() {
        return $this->teacherRepository->getStudentsWithFinalMarks(
            $this->getDataModel()->getId()
        );
    }

    public function getFinalMark($studentId, $subjectId) {
        return $this->teacherRepository->getFinalMark(
            $this->getDataModel()->getId(),
            intval($studentId),
            intval($subjectId)
        );
    }

    /**
     * Returns suggested final mark (rounded or not)
     *
     * @param boolean $round
     * @return float|int
     */
    public function getSuggestedFinalMark($round = true) {
        $marks = $this->getSubjectMarks($this->getSubject()->subject_id);
        $markSum = 0;
        foreach($marks as $mark) {
            $markSum += $mark->mark;
        }
        $suggestMark = $markSum / count($marks);

        if($round) {
            return round($suggestMark);
        }

        return round($suggestMark, 2);
    }

    /**
     * Sets current request student view model
     *
     * @param StudentViewModel $studentViewModel
     * @return self
     */
    public function setStudentViewModel(StudentViewModel $studentViewModel): self {
        $this->studentViewModel = $studentViewModel;
        return $this;
    }

    /**
     * Returns current request student view model
     *
     * @return StudentViewModel|null
     */
    public function getStudentViewModel(): ?StudentViewModel {
        return $this->studentViewModel;
    }

    /**
     * Sets current request subject
     *
     * @param mixed $subject
     * @return self
     */
    public function setSubject($subject): self {
        $this->subject = $subject;
        return $this;
    }

    /**
     * Returns current request subject
     *
     * @return mixed
     */
    public function getSubject() {
        return $this->subject;
    }

}