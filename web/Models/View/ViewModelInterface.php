<?php declare(strict_types = 1);

namespace App\Models\View;

use App\Models\Data\EntityInterface;

interface ViewModelInterface {

    /**
     * Loads data model by id
     *
     * @param int $id
     * @return EntityInterface|null
     */
    public function loadDataModel(int $id) : ?EntityInterface;

    /**
     * Sets underlying data model
     *
     * @param EntityInterface $model
     * @return self
     */
    public function setDataModel(EntityInterface $model);

    /**
     * Returns underlying data model
     *
     * @return EntityInterface|null
     */
    public function getDataModel() : ?EntityInterface;

}