<?php declare(strict_types = 1);

namespace App\Services;

use App\Core\Session\Session;
use App\Services\Repository\UserRepository;
use App\Services\ServiceManager;
use App\Models\Data\UserData;
use App\Core\Session\FileSessionStorage;

class AccountManager {

    const PASSWORD_ALGORITHM = "sha512";

    /** @var Session */
    private $session;

    /** @var UserRepository */
    private $userRepository;

    /** @var FileSessionStorage */
    private $fileSessionStorage;

    public function __construct()
    {
        $this->session = ServiceManager::getService(Session::class);
        $this->userRepository = ServiceManager::getService(UserRepository::class);
        $this->session = ServiceManager::getService(Session::class);
        $this->fileSessionStorage = ServiceManager::getService(FileSessionStorage::class);
    }

    public function isLoggedIn() {
        return $this->session->get('uid');
    }

    public function adminLogin($username, $password): UserData {
        return $this->_login($username, $password, true);
    }

    public function login($username, $password) : UserData {
        return $this->_login($username, $password, false);
    }

    protected function _login($username, $password, $isAdminLogin = false): UserData {
        /** @var UserData */
        $user = $this->userRepository->getByUsername($username);

        if(!$user) {
            throw new \Exception("Ne postoji ovaj korisnik");
        }

        $adminCheck = $isAdminLogin ? ($user->getType() === "admin") : ($user->getType() !== "admin");
        if(!$adminCheck) {
            throw new \Exception("Ne postoji ovaj korisnik");
        }

        if(!$this->validatePassword($password, $user)) {
            throw new \Exception("Neispravna lozinka");
        }

        $userType = $user->getType();

        unset($_COOKIE['APPSESSION']);
        $this->session->put('uid', $user->getId());
        $this->session->put('user_data', [
            'uid' => $user->getId(),
            'user_type' => $userType
        ]);
        $this->session->save();

        return $user;
    }

    public function logout() {
        $sessionId  = $_COOKIE['APPSESSION'];
        $this->fileSessionStorage->delete($sessionId);
        $_COOKIE['APPSESSION'] = "";
    }

    private function validatePassword(
        string $password,
        UserData $user
    ) {
        if( $this->generatePasswordHash($password) == $user->getPasswordHash()) {
            return true;
        }

        return false;
    }

    public function generatePasswordHash(string $password) {
        return hash(self::PASSWORD_ALGORITHM, $password);
    }

}