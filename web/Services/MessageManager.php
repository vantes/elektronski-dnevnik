<?php declare(strict_types = 1);

namespace App\Services;

use App\Core\Session\Session;
use App\Models\Message;
use App\Services\ServiceManager;

/**
 * Session based message storage
 */
class MessageManager {

    const MESSAGE_TYPE_SUCCESS = "success";
    const MESSAGE_TYPE_ERROR = "danger";

    /** @var Message[]|array */
    private $messages = [];

    /** @var Session $session */
    private $session;

    public function __construct() {
        $this->session = ServiceManager::getService(Session::class);
    }

    public function loadMessages() {
        /** @var array $messages */
        $messages = $this->session->get('messages') ?: [];
        foreach($messages as $message) {
            if(!isset($message->type, $message->message)) {
                continue;
            }

            /** @var array $message */
            $this->addMessage($message->type, $message->message);
        }
    }

    /**
     *
     * @param string $type
     * @param string $message
     * @return void
     */
    public function addMessage(string $type, string $message) {
        $messageObj = new Message();
        $messageObj->setType($type);
        $messageObj->setMessage($message);

        $this->messages[] = $messageObj;
        $this->session->set('messages', $this->messages);
        $this->session->save();

        return $this;
    }

    /**
     *
     * @return Message[]|array
     */
    public function getMessages($clearMessages = true) {
        $messages = $this->messages;
        if($clearMessages) {
            $this->messages = [];
            $this->session->set('messages', $this->messages);
            $this->session->save();
        }
        return $messages;
    }

}