<?php declare(strict_types = 1);

namespace App\Services\Repository;

use App\Models\Data\EntityInterface;
use App\Services\ServiceManager;
use App\Core\DatabaseConnection;
use PDO;

/**
 * Handles most read / write operations for the entities in the system
 */
abstract class AbstractRepository implements RepositoryInterface {

    const DATE_FORMAT = "Y-m-d H:i:s";

    /** @var DatabaseConnection */
    protected $dbConnection;

    public function __construct() {
        $this->dbConnection = ServiceManager::getService(DatabaseConnection::class);
    }

    protected function convertDate(string $dateString): \DateTime {
        return \DateTime::createFromFormat(self::DATE_FORMAT, $dateString);
    }

    /**
     * Method called before executing save on SQL
     *
     * @param EntityInterface $entity
     * @return self
     */
    protected function beforeSave(EntityInterface $entity) {
        return $this;
    }

    /**
     * Method called before executing save on SQL
     *
     * @param EntityInterface $entity
     * @return self
     */
    protected function afterSave(EntityInterface $entity) {
        return $this;
    }

    /**
     * Method called after loading an entity
     * Usefull for setting up additional relations / data
     *
     * @param EntityInterface $entity
     * @return self
     */
    protected function afterLoad(EntityInterface $entity) {
        return $this;
    }

    /**
     * Returns entity by ID
     *
     * @param int $id
     * @return EntityInterface|null
     */
    public function getById(int $id): ?EntityInterface {
        $sql = 'SELECT * FROM ' . $this->getTableName() . ' WHERE ' . $this->getColumnId() . ' = ?;';
        $preparedStatement = $this->dbConnection->getConnection()->prepare($sql);
        $result = $preparedStatement->execute([ $id ]);

        $entity = null;
        if($result) {
            $object = $preparedStatement->fetch(\PDO::FETCH_OBJ);
            if($object) {
                $entity = $this->createEntity($object);
                $this->afterLoad($entity);
            }
        }

        return $entity;
    }

    /**
     * Saves entity
     *
     * @param EntityInterface $entity
     * @return bool
     */
    public function save(EntityInterface $entity): bool {
        $this->beforeSave($entity);

        if($entity->getId() && $this->getById($entity->getId())) {
            return $this->update($entity);
        } else {
            return $this->create($entity);
        }
    }

    protected function update(EntityInterface $entity): bool {
        $sql = "";

        $fields = $entity->getDbFields();

        $sql = 'UPDATE ' . $this->getTableName() . " SET ";
        $i = 1;
        foreach($fields as $fieldName => $fieldValue) {
            $sql .= $fieldName . " = " . ":$fieldName";
            if($i < count($fields)) $sql .= ", ";
            $i++;
        }

        $fields["id"] = $entity->getId();
        $sql .= ' WHERE ' . $this->getColumnId() . ' = :id ;';

        $preparedStatement = $this->dbConnection->getConnection()->prepare($sql);
        $result = $preparedStatement->execute($fields);

        if($result) {
            $this->afterSave($entity);
        }

        return $result;
    }

    protected function create(EntityInterface $entity): bool {
        $sql = "";

        $fields = $entity->getDbFields();
        $fieldNames = array_keys($fields);

        $sql = 'INSERT INTO ' . $this->getTableName() . '(' . implode(",", $fieldNames) . ')' . ' VALUES (' . implode(",", array_map(function($value) {
           return ":" . $value;
        }, array_keys($fields))) . ');';

        $preparedStatement = $this->dbConnection->getConnection()->prepare($sql);
        $result = $preparedStatement->execute($fields);

        if($result) {
            /** @var \stdClass $object */
            $objectId = $this->dbConnection->getConnection()->lastInsertId();

            $entity->setId( intval($objectId) );

            $this->afterSave($entity);
        }

        return $result;
    }

    /**
     * Deletes entity, returns indication of success
     *
     * @param EntityInterface $entity
     * @return bool
     */
    public function delete(EntityInterface $entity): bool {
        return $this->deleteById($entity->getId());
    }

    /**
     * Deletes entity by id, returns indication of success
     *
     * @param integer $id
     * @return bool
     */
    public function deleteById(int $id): bool {
        $entity = $this->getById($id);

        if(!$entity || !$entity->getId()) {
            throw new \Exception("No such entity exists");
        }
        $entity->setDeleted(true);

        $sql = 'UPDATE ' . $this->getTableName() . ' SET ' . $this->getTableName() . '.deleted = 1 WHERE ' . $this->getColumnId() . ' = :id;';
        $preparedStatement = $this->dbConnection->getConnection()->prepare($sql);
        $result = $preparedStatement->execute([ 'id' => $id ]);

        $this->afterSave($entity);

        return $result;
    }

    /**
     * Restores entity by id, returns indication of success
     *
     * @param integer $id
     * @return bool
     */
    public function restoreById(int $id): bool {
        $entity = $this->getById($id);

        if(!$entity || !$entity->getId()) {
            throw new \Exception("No such entity exists");
        }

        $entity->setDeleted(false);

        $sql = 'UPDATE ' . $this->getTableName() . ' SET ' . $this->getTableName() . '.deleted = 0 WHERE ' . $this->getColumnId() . ' = :id;';
        $preparedStatement = $this->dbConnection->getConnection()->prepare($sql);
        $result = $preparedStatement->execute([ 'id' => $id ]);

        $this->afterSave($entity);

        return $result;
    }

    protected function getJoinSql(): string {
        return "";
    }

    /**
     * Returns deleted SQL fileters (fields) for search queries
     * Example: do not return deleted entities from the system for normal users
     *
     * @return array
     */
    protected function getDefaultFilters(): array {
        return ['deleted' => 0];
    }

    /**
     * Returns total number of rows in the table
     *
     * @return integer
     */
    public function count(array $filters, bool $applyDefaultFilters = true): int {
        $sql = 'SELECT COUNT(*) FROM ' . $this->getTableName() . $this->getJoinSql();

        $defaultFilters = $applyDefaultFilters ? $this->getDefaultFilters() : [];
        $filters = array_merge($defaultFilters, $filters);

        if(count($filters)) {
            $sql .= ' WHERE ';
            $i = 1;
            foreach($filters as $column => $value) {
                $sql .= " " . $this->getTableName() . ".$column = :$column";
                if($i < count($filters)) {
                    $sql .= " AND ";
                }
                $i++;
            }
        }

        $preparedStatement = $this->dbConnection->getConnection()->prepare($sql);
        $result = $preparedStatement->execute($filters);

        if($result) {
            return intval($preparedStatement->fetchColumn());
        }

        return 0;
    }

    /**
     * Finds entities by filters
     *
     * @param array $filters
     * @return EntityInterface[]|null
     */
    public function search(array $filters, int $page = 0, int $limit = 9999, bool $applyDefaultFilters = true): ?array {
        $sql = 'SELECT * FROM ' . $this->getTableName() . $this->getJoinSql();

        $defaultFilters = $applyDefaultFilters ? $this->getDefaultFilters() : [];
        $appliedFilters = [];

        foreach($defaultFilters as $column => $value) {
            if(isset($filters[$column])) {
                unset($defaultFilters);
            }
        }

        $filterCount = count($defaultFilters) + count($filters);
        if($filterCount) {
            $sql .= ' WHERE ';
        }

        if(count($defaultFilters)) {
            foreach($defaultFilters as $column => $value) {
                $sql .= " " . $this->getTableName() . ".$column = :$column";
                $appliedFilters[$column] = $value;
                if(count($appliedFilters) < $filterCount) {
                    $sql .= " AND ";
                }
            }
        }
        if(count($filters)) {
            foreach($filters as $column => $value) {
                $sql .= " " . "$column = :$column";
                $appliedFilters[$column] = $value;
                if(count($appliedFilters) < $filterCount) {
                    $sql .= " AND ";
                }
            }
        }

        $offset = intval($page * $limit);
        $limit = intval($limit);
        $sql .= " LIMIT $limit OFFSET $offset";


        $this->dbConnection->getConnection()->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
        $preparedStatement = $this->dbConnection->getConnection()->prepare($sql);
        $result = $preparedStatement->execute($appliedFilters);

        $entities = [];

        if($result) {
            $objects = $preparedStatement->fetchAll(\PDO::FETCH_OBJ);
            foreach($objects as $object) {
                $entity = $this->createEntity($object);
                $this->afterLoad($entity);
                $entities[] = $entity;
            }
        }
        return $entities;
    }

}