<?php declare(strict_types = 1);

namespace App\Services\Repository;

use App\Services\Repository\RepositoryInterface;
use App\Models\Data\EntityInterface;
use App\Models\Data\MarkData;
use App\Services\Repository\AbstractRepository;
use App\Services\ServiceManager;

class MarkRepository extends AbstractRepository implements RepositoryInterface
{
    const TABLE_NAME = 'mark';
    const COLUMN_ID = 'id';

    /** @var StudentRepository */
    private $studentRepository;

    /** @var TeacherRepository */
    private $teacherRepository;

    /** @var SubjectRepository */
    private $subjectRepository;

    public function __construct() {
        parent::__construct();
        $this->studentRepository = ServiceManager::getService(StudentRepository::class);
        $this->teacherRepository = ServiceManager::getService(TeacherRepository::class);
        $this->subjectRepository = ServiceManager::getService(SubjectRepository::class);
    }

    /**
     * @inheritDoc
     *
     * @return string
     */
    public function getTableName(): string {
        return self::TABLE_NAME;
    }

    /**
     * @inheritDoc
     *
     * @return string
     */
    public function getColumnId(): string
    {
        return self::COLUMN_ID;
    }

    /**
     * @inheritDoc
     *
     * @param \stdClass $object
     * @return EntityInterface
     */
    public function createEntity(\stdClass $object): EntityInterface {
        $entity = new MarkData();
        $entity->setId( intval($object->{$this->getColumnId()}) );
        $entity->setStudentId( intval($object->student_id) );
        $entity->setTeacherId( intval($object->teacher_id) );
        $entity->setSubjectId( intval($object->subject_id) );
        $entity->setSchoolYearId( intval($object->school_year) );
        $entity->setMark( intval($object->mark) );
        $entity->setQuarter( intval($object->quarter) );
        $entity->setCreatedAt( $this->convertDate($object->created_at) );
        $entity->setDeleted( boolval($object->deleted) );

        return $entity;
    }

    protected function afterLoad(\App\Models\Data\EntityInterface $entity)
    {
        /** @var \App\Models\Data\MarkData $entity */
        $entity->setStudent(
            $this->studentRepository->getById($entity->getStudentId())
        );
        $entity->setTeacher(
            $this->teacherRepository->getById($entity->getTeacherId())
        );
        $entity->setSubject(
            $this->subjectRepository->getById($entity->getStudentId())
        );
    }

}