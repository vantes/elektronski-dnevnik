<?php declare(strict_types = 1);

namespace App\Services\Repository;

use App\Models\Data\EntityInterface;

interface RepositoryInterface {

    /**
     * Returns MySQL table name
     *
     * @return string
     */
    public function getTableName(): string;

    /**
     * Returns the name of the primary key column in the table
     *
     * @return string
     */
    public function getColumnId(): string;

    /**
     * Creates entity using the returned raw DB object
     *
     * @param \stdClass $object
     * @return EntityInterface|null
     */
    public function createEntity(\stdClass $object): ?EntityInterface;

    /**
     * Returns entity by ID
     *
     * @param int $id
     * @return EntityInterface|null
     */
    public function getById(int $id): ?EntityInterface;

    /**
     * Saves entity
     *
     * @param EntityInterface $entity
     * @return bool
     */
    public function save(EntityInterface $entity): bool;

    /**
     * Deletes entity
     *
     * @param EntityInterface $entity
     * @return void
     */
    public function delete(EntityInterface $entity);

    /**
     * Deletes entity by id
     *
     * @param integer $id
     * @return void
     */
    public function deleteById(int $id);

    /**
     * Finds entities by filters
     *
     * @param array $filters
     * @param int $page
     * @param int $limit
     * @param bool $applyDefaultFilters
     * @return EntityInterface[]|null
     */
    public function search(array $filters, int $page = 0, int $limit = 9999, bool $applyDefaultFilters): ?array;

}