<?php declare(strict_types = 1);

namespace App\Services\Repository;

use App\Services\Repository\RepositoryInterface;
use App\Models\Data\EntityInterface;
use App\Models\Data\SchoolYear;
use App\Services\Repository\AbstractRepository;

class SchoolYearRepository extends AbstractRepository implements RepositoryInterface
{
    const TABLE_NAME = 'school_year';
    const COLUMN_ID = 'id';

    /**
     * @inheritDoc
     *
     * @return string
     */
    public function getTableName(): string {
        return self::TABLE_NAME;
    }

    /**
     * @inheritDoc
     *
     * @return string
     */
    public function getColumnId(): string
    {
        return self::COLUMN_ID;
    }

}