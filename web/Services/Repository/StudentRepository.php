<?php declare(strict_types = 1);

namespace App\Services\Repository;

use App\Services\Repository\RepositoryInterface;
use App\Models\Data\EntityInterface;
use App\Models\Data\StudentData;
use App\Models\Data\UserData;
use App\Services\Repository\AbstractRepository;
use App\Services\ServiceManager;

class StudentRepository extends AbstractRepository implements RepositoryInterface {

    const TABLE_NAME = 'student';
    const COLUMN_ID = 'id';

    /** @var UserRepository */
    protected $userRepository;

    /** @var SubjectRepository */
    protected $subjectRepository;

    public function __construct() {
        parent::__construct();
        $this->userRepository = ServiceManager::getService(UserRepository::class);
        $this->subjectRepository = ServiceManager::getService(SubjectRepository::class);
    }

    /**
     * @inheritDoc
     *
     * @return string
     */
    public function getTableName(): string {
        return self::TABLE_NAME;
    }

    /**
     * @inheritDoc
     *
     * @return string
     */
    public function getColumnId(): string
    {
        return self::COLUMN_ID;
    }

    protected function getJoinSql(): string {
        return " INNER JOIN user ON student.user_id = user.uid ";
    }

    /**
     * Method called before executing save on SQL
     *
     * @param EntityInterface $entity
     * @return self
     */
    protected function beforeSave(EntityInterface $entity) {
        /** @var StudentData $entity */
        if(!$entity->getUserId()) {
            $entity->getUser()->setType("student");
            if(!$this->userRepository->save($entity->getUser())) {
                throw new \Exception("Could not save user");
            }
            $entity->setUserId($entity->getUser()->getId());
        }
        return $this;
    }

    /**
     * Method called after executing save on SQL
     *
     * @param EntityInterface $entity
     * @return self
     */
    protected function afterSave(EntityInterface $entity) {
        /** @var StudentData $entity */
        /** @var UserData $user */
        $user = $this->userRepository->getById( $entity->getUserId() );
        $user->setDeleted($entity->isDeleted());
        if(!$this->userRepository->save($user)) {
            throw new \Exception("Could not save user");
        }
        return $this;
    }

    /**
     * @inheritDoc
     *
     * @param \stdClass $object
     * @return EntityInterface
     */
    public function createEntity(\stdClass $object): EntityInterface {
        $entity = new StudentData();
        $entity->setId( intval($object->{$this->getColumnId()}) );
        $entity->setForename($object->forename);
        $entity->setSurname($object->surname);
        $entity->setAddress($object->address);
        $entity->setPhone($object->phone);
        $entity->setDivision($object->division);
        $entity->setYear($object->year);
        $entity->setGender($object->gender);
        $entity->setDeleted( boolval($object->deleted) );
        $entity->setUserId( intval($object->user_id) );

        $user = $this->userRepository->getById( $entity->getUserId() );
        if(!$user) {
            throw new \Exception("Could not load related user entity");
        }

        $entity->setUser($user);

        return $entity;
    }

    /**
     * Returns array of subjects listened by student
     *
     * @param integer $studentId
     * @return array
     */
    public function getStudentSubjects(int $studentId, $teacherId = null): array {
        $sql = 'SELECT *
                FROM subject INNER JOIN student_listens ON
                subject.id = student_listens.subject_id WHERE student_listens.student_id = :student_id';

        $params = ['student_id' => $studentId];

        if($teacherId) {
            $sql .= " AND student_listens.teacher_id = :teacher_id";
            $params['teacher_id']  = $teacherId;
        }
        $prep = $this->dbConnection->getConnection()->prepare($sql);
        $res = $prep->execute($params);

        $subjects = [];

        if($res) {
            $subjects = $prep->fetchAll(\PDO::FETCH_OBJ);
        }

        return $subjects;
    }


    /**
     * Returns student marks/grades for given subject and trimester/quarter
     *
     * @param integer $studentId
     * @param integer $subjectId
     * @param integer $trimester
     * @return MarkData[]
     */
    public function getMarks(int $studentId, int $subjectId): array {
        $sql = 'SELECT *
                FROM mark
                WHERE subject_id = :subject_id
                AND student_id = :student_id
                ORDER BY quarter, created_at ASC';

        $prep = $this->dbConnection->getConnection()->prepare($sql);
        $res = $prep->execute([
            'subject_id' => $subjectId,
            'student_id' => $studentId
        ]);

        $marks = [];

        if($res) {
            $marks = $prep->fetchAll(\PDO::FETCH_OBJ);
        }

        return $marks;
    }

    /**
     * Returns student marks/grades for given subject and trimester/quarter
     *
     * @param integer $studentId
     * @param integer $subjectId
     * @param integer $trimester
     * @return MarkData[]
     */
    public function getTrimesterGrades(int $studentId, int $subjectId, $trimester): array {
        $sql = 'SELECT mark
                FROM subject INNER JOIN mark ON
        subject.id = mark.subject_id WHERE subject.id = :subject_id AND mark.student_id = :student_id AND mark.quarter = :quarter';
        $prep = $this->dbConnection->getConnection()->prepare($sql);
        $res = $prep->execute([
            'subject_id' => $subjectId,
            'student_id' => $studentId,
            'quarter' => $trimester
        ]);

        $marks = [];

        if($res) {
            $marks = $prep->fetchAll(\PDO::FETCH_OBJ);
        }

        return $marks;
    }

    /**
     * Returns average grade for given student / subject / trimester
     *
     * @param integer $studentId
     * @param integer $subjectId
     * @param integer $trimester
     * @return float|null
     */
    public function getTrimesterAverageGrade(int $studentId, int $subjectId, int $trimester): ?float {
        $sql = 'SELECT AVG(mark)
                FROM subject INNER JOIN mark ON
        subject.id = mark.subject_id WHERE subject.id = :subject_id AND mark.student_id = :student_id AND mark.quarter = :quarter';
        $prep = $this->dbConnection->getConnection()->prepare($sql);
        $res = $prep->execute([
            'subject_id' => $subjectId,
            'student_id' => $studentId,
            'quarter' => $trimester
        ]);

        $avg = NULL;

        if($res) {
            $avg = floatval($prep->fetchColumn());
        }

        return $avg;
    }

    public function getSubjectAverageGrade(int $subjectId, int $studentId): ?float {
        $sql = 'SELECT AVG(mark)
                FROM subject INNER JOIN mark ON
        subject.id = mark.subject_id WHERE subject.id = :subject_id AND mark.student_id = :student_id';
        $prep = $this->dbConnection->getConnection()->prepare($sql);
        $res = $prep->execute([
            'subject_id' => $subjectId,
            'student_id' => $studentId
        ]);

        $avg = NULL;

        if($res) {
            $avg = floatval($prep->fetchColumn());
        }

        return $avg;
    }

    /**
     * Returns final mark
     *
     * @param integer $teacherId
     * @param integer $studentId
     * @param integer $subjectId
     * @return mixed|null
     */
    public function getFinalMark(
        int $studentId,
        int $subjectId
    ) {
        $sql = "SELECT * FROM final_mark
                WHERE student_id = :student_id
                AND subject_id = :subject_id

        ";

        $prep = $this->dbConnection->getConnection()->prepare($sql);

        $result = $prep->execute([
            'student_id' => $studentId,
            'subject_id' => $subjectId
        ]);

        if ($result) {
            return $prep->fetchColumn(4);
        }

        return null;
    }

}