<?php declare(strict_types = 1);

namespace App\Services\Repository;

use App\Services\Repository\RepositoryInterface;
use App\Models\Data\EntityInterface;
use App\Models\Data\SubjectData;
use App\Services\Repository\AbstractRepository;

class SubjectRepository extends AbstractRepository implements RepositoryInterface
{
    const TABLE_NAME = 'subject';
    const COLUMN_ID = 'id';

    /**
     * @inheritDoc
     *
     * @return string
     */
    public function getTableName(): string {
        return self::TABLE_NAME;
    }

    /**
     * @inheritDoc
     *
     * @return string
     */
    public function getColumnId(): string
    {
        return self::COLUMN_ID;
    }

    /**
     * @inheritDoc
     *
     * @param \stdClass $object
     * @return EntityInterface
     */
    public function createEntity(\stdClass $object): EntityInterface {
        $entity = new SubjectData();
        $entity->setId( intval($object->{$this->getColumnId()}) );
        $entity->setName( $object->name );
        $entity->setDescription( $object->description );

        $entity->setDeleted( boolval($object->deleted) );
        return $entity;
    }


}