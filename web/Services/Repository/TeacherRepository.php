<?php declare(strict_types = 1);

namespace App\Services\Repository;

use App\Services\Repository\RepositoryInterface;
use App\Models\Data\EntityInterface;
use App\Models\Data\TeacherData;
use App\Models\Data\UserData;
use App\Services\Repository\AbstractRepository;
use App\Services\SchoolYearResolver;
use App\Models\Data\SchoolYear;
use App\Services\ServiceManager;

class TeacherRepository extends AbstractRepository implements RepositoryInterface {

    const TABLE_NAME = 'teacher';
    const COLUMN_ID = 'id';

    /** @var SchoolYearResolver */
    protected $schoolYearResolver;

    /** @var UserRepository */
    protected $userRepository;

    public function __construct() {
        parent::__construct();
        $this->schoolYearResolver = ServiceManager::getService(SchoolYearResolver::class);
        $this->userRepository = ServiceManager::getService(UserRepository::class);
    }

    /**
     * @inheritDoc
     *
     * @return string
     */
    public function getTableName(): string {
        return self::TABLE_NAME;
    }

    /**
     * @inheritDoc
     *
     * @return string
     */
    public function getColumnId(): string
    {
        return self::COLUMN_ID;
    }

    protected function getJoinSql(): string {
        return " INNER JOIN user ON teacher.user_id = user.uid ";
    }

    /**
     * Method called before executing save on SQL
     *
     * @param EntityInterface $entity
     * @return self
     */
    protected function beforeSave(EntityInterface $entity) {
        /** @var TeacherData $entity */
        if(!$entity->getUserId()) {
            $entity->getUser()->setType("teacher");
            if(!$this->userRepository->save($entity->getUser())) {
                throw new \Exception("Could not save user");
            }
            $entity->setUserId($entity->getUser()->getId());
        }
        return $this;
    }

    /**
     * Method called after executing save on SQL
     *
     * @param EntityInterface $entity
     * @return self
     */
    protected function afterSave(EntityInterface $entity) {
        /** @var TeacherData $entity */
        /** @var UserData $user */
        $user = $entity->getUser() ?: $this->userRepository->getById( $entity->getUserId() );
        $user->setDeleted($entity->isDeleted());
        if(!$this->userRepository->save($user)) {
            throw new \Exception("Could not save user");
        }

        if(!$this->assignSubjects($entity->getId(), $entity->getSubjectIds())) {
            throw new \Exception("Could not save subjects");
        }

        return $this;
    }

    /**
     * @inheritDoc
     *
     * @param \stdClass $object
     * @return EntityInterface
     */
    public function createEntity(\stdClass $object): EntityInterface {
        $entity = new TeacherData();
        $entity->setId( intval($object->{$this->getColumnId()}) );
        $entity->setForename($object->forename);
        $entity->setSurname($object->surname);
        $entity->setQualifications($object->qualifications);
        $entity->setGender($object->gender);
        $entity->setDeleted( boolval($object->deleted) );
        $entity->setUserId( intval($object->user_id) );

        if ($entity->getId()) {
            $subjectIds = array_map(function ($subject) {
                return $subject->subject_id;
            }, $this->getAssignedSubjects($entity->getId()));
            $entity->setSubjectIds($subjectIds);
        }

        $user = $this->userRepository->getById( intval($object->user_id) );
        if(!$user) {
            throw new \Exception("Učitavanje povezanog korisničkog entiteta nije uspelo");
        }
        $entity->setUser($user);

        return $entity;
    }

    /**
     * Returns all assigned subjects or a single one if a subject id is provided in form of an array
     *
     * @param int $teacherId
     * @param int|null $subjectId
     * @return array
     */
    public function getAssignedSubjects(int $teacherId, $subjectId = null): array {
        $sql = 'SELECT *
                FROM subject INNER JOIN assigned_subjects ON
                subject.id = assigned_subjects.subject_id
                WHERE assigned_subjects.teacher_id = :teacher_id';

        $params = ['teacher_id' => $teacherId];

        if ($subjectId) {
            $sql .= " AND subject.id = :subject_id";
            $params['subject_id'] = $subjectId;
        }

        $prep = $this->dbConnection->getConnection()->prepare($sql);

        $res = $prep->execute($params);

        $subjects = [];

        if($res) {
            $subjects = $prep->fetchAll(\PDO::FETCH_OBJ);
        }

        return $subjects;
    }

    public function assignSubjects(int $teacherId, array $subjectIds) : bool {
        $assignedSubjects = $this->getAssignedSubjects($teacherId);
        $assignedSubjectIds = array_map(function($subject) {
            return $subject->subject_id;
        }, $assignedSubjects);
        $intersectIds = array_intersect($assignedSubjectIds, $subjectIds);

        // current: 1, 3, 5
        // new: 2, 3, 4
        // insert = 2, 4 -> new - intersect
        // delete = 1, 5 -> current - intersect

        $insertIds = array_diff($subjectIds, $intersectIds);
        $deleteIds = array_diff($assignedSubjectIds, $intersectIds);

        $params = [
            'teacher_id' => $teacherId
        ];

        $sql = "";

        if(count($deleteIds)) {
            foreach ($deleteIds as $i => $deleteId) {
                $sql .= "DELETE FROM assigned_subjects
                    WHERE teacher_id = :teacher_id AND subject_id IN (:deleteId" . $i . ");";
                $params['deleteId' . $i] = $deleteId;
            }
        }

        if(count($insertIds)) {
            foreach($insertIds as $i => $insertId) {
                $sql .= "INSERT INTO assigned_subjects (teacher_id, subject_id)
                    VALUES (:teacher_id, :insertId" . $i . ");";
                $params['insertId' . $i] = $insertId;
            }
        }

        if(!count($insertIds) && !count($deleteIds)) {
            return true;
        }

        $prep = $this->dbConnection->getConnection()->prepare($sql);
        $res = $prep->execute($params);

        return $res;
    }


    public function getStudentsWithFinalMarks($teacherId): array {
        $sql = 'SELECT *
                FROM student INNER JOIN final_mark ON
                student.id = final_mark.student_id
                LEFT JOIN subject ON
                subject.id = final_mark.subject_id
                WHERE final_mark.teacher_id = :teacher_id';

        $prep = $this->dbConnection->getConnection()->prepare($sql);

        $res = $prep->execute([
            'teacher_id' => $teacherId
        ]);

        $studentsWithFinalMarks = [];

        if($res) {
            $studentsWithFinalMarks = $prep->fetchAll(\PDO::FETCH_OBJ);
        }

        return $studentsWithFinalMarks;
    }

    public function checkIfStudentListens(
        int $teacherId,
        int $studentId,
        int $subjectId
    ): bool {
        $sql = "SELECT * FROM student_listens
                WHERE student_id = :student_id
                AND subject_id = :subject_id
                AND teacher_id = :teacher_id
        ";

        $prep = $this->dbConnection->getConnection()->prepare($sql);

        $result = $prep->execute([
            'teacher_id' => $teacherId,
            'student_id' => $studentId,
            'subject_id' => $subjectId
        ]);

        if($result) {
            return count($prep->fetchAll()) > 0;
        }

        return false;
    }

    public function createMark(
        int $teacherId,
        int $studentId,
        int $subjectId,
        int $mark
    ): bool {

        // check if student actually listens to teacher subject and is related to teacher
        if(
            !$this->checkIfStudentListens(
                $teacherId,
                $studentId,
                $subjectId
            ) ||
            $mark < 1 ||
            $mark > 5
        ) {
            return false;
        }

        /** @var SchoolYear $schoolYear */
        $schoolYear = $this->schoolYearResolver->getActiveSchoolYear();
        $activeTrimester = $schoolYear->getActiveTrimester();

        $sql = "INSERT INTO mark (teacher_id, student_id, subject_id, mark, created_at, school_year, quarter)
                VALUES (:teacher_id, :student_id, :subject_id, :mark, :created_at, :school_year, :quarter)";


        $quarter = $activeTrimester['index'];

        $prep = $this->dbConnection->getConnection()->prepare($sql);

        $res = $prep->execute([
            'teacher_id' => $teacherId,
            'student_id' => $studentId,
            'subject_id' => $subjectId,
            'mark' => $mark,
            'school_year' => $schoolYear->getId(),
            'quarter' => $quarter,
            'created_at' => date('Y-m-d H:i:s')
        ]);

        return $res;
    }

    public function createFinalMark(
        int $teacherId,
        int $studentId,
        int $subjectId,
        int $mark,
        float $suggestedFinalMark,
        string $reason
    ): bool {

        // check if student actually listens to teacher subject and is related to teacher
        if(
            !$this->checkIfStudentListens(
                $teacherId,
                $studentId,
                $subjectId
            ) ||
            $mark < 1 ||
            $mark > 5 ||
            $mark > $suggestedFinalMark && !$reason ||
            $mark < $suggestedFinalMark
            // add check if final mark exists already
        ) {
            return false;
        }

        /** @var SchoolYear $schoolYear */
        $schoolYear = $this->schoolYearResolver->getActiveSchoolYear();

        $sql = "INSERT INTO final_mark (teacher_id, student_id, subject_id, final_mark, explanation, date, school_year)
                VALUES (:teacher_id, :student_id, :subject_id, :mark, :reason, :date, :school_year)
        ";

        $prep = $this->dbConnection->getConnection()->prepare($sql);

        $res = $prep->execute([
            'teacher_id' => $teacherId,
            'student_id' => $studentId,
            'subject_id' => $subjectId,
            'mark' => $mark,
            'reason' => $reason,
            'date' => date("Y-m-d H:i:s"),
            'school_year' => $schoolYear->getId()
        ]);

        return $res;
    }

    /**
     * Returns final mark
     *
     * @param integer $teacherId
     * @param integer $studentId
     * @param integer $subjectId
     * @return mixed|bool
     */
    public function getFinalMark(
        int $teacherId,
        int $studentId,
        int $subjectId
    ) {
        $sql = "SELECT * FROM final_mark
                WHERE student_id = :student_id
                AND subject_id = :subject_id
                AND teacher_id = :teacher_id
        ";

        $prep = $this->dbConnection->getConnection()->prepare($sql);

        $result = $prep->execute([
            'teacher_id' => $teacherId,
            'student_id' => $studentId,
            'subject_id' => $subjectId
        ]);

        if ($result) {
            return $prep->fetchObject();
        }

        return false;
    }

}