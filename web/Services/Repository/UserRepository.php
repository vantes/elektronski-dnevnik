<?php declare(strict_types = 1);

namespace App\Services\Repository;

use App\Services\Repository\RepositoryInterface;
use App\Models\Data\EntityInterface;
use App\Models\Data\UserData;
use App\Services\Repository\AbstractRepository;

class UserRepository extends AbstractRepository implements RepositoryInterface {

    const USER_TYPE_STUDENT = 'student';
    const USER_TYPE_TEACHER = 'teacher';
    const USER_TYPE_ADMIN   = 'admin';

    const TABLE_NAME = 'user';
    const COLUMN_ID = 'uid';

    /**
     * @inheritDoc
     *
     * @return string
     */
    public function getTableName(): string {
        return self::TABLE_NAME;
    }

    /**
     * @inheritDoc
     *
     * @return string
     */
    public function getColumnId(): string {
        return self::COLUMN_ID;
    }

    /**
     * @inheritDoc
     *
     * @param \stdClass $object
     * @return EntityInterface
     */
    public function createEntity($object): EntityInterface {
        $entity = new UserData();
        $entity->setId( intval($object->{$this->getColumnId()}) );
        $entity->setCreatedAt(\DateTime::createFromFormat("Y-m-d H:i:s", $object->created_at));
        $entity->setEmail($object->email);
        $entity->setPasswordHash($object->password_hash);
        $entity->setType($object->type);
        $entity->setDeleted( boolval($object->deleted) );
        return $entity;
    }

    /**
     * Returns user by username
     *
     * @param string $username
     * @return EntityInterface|null
     */
    public function getByUsername(string $username): ?EntityInterface {
        $users = $this->search([
            'email' => $username
        ]);
        if(count($users)) {
            return array_shift($users);
        }
        return null;
    }

}