<?php declare(strict_types = 1);

namespace App\Services;

use App\Models\Http\RequestInterface;
use App\Models\Http\Request;

/**
 * Instead of using request variables directly we use this factory class to
 * create a object oriented object with already filtered parameters
 */
class RequestFactory {

    public static function createRequest(): RequestInterface {
        $request = new Request();

        foreach($_GET as $index => $value) {
            $request->setParam($index, $value, Request::PARAM_TYPE_GET);
        }

        foreach($_POST as $index => $value) {
            $request->setParam($index, $value, Request::PARAM_TYPE_POST);
        }

        $request->setServerInfo($_SERVER);

        return $request;
    }

}