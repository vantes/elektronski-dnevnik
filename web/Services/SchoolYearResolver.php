<?php declare(strict_types = 1);

namespace App\Services;

use App\Services\ServiceManager;
use App\Core\DatabaseConnection;
use App\Models\Data\SchoolYear;

class SchoolYearResolver {

    const DATE_FORMAT = "Y-m-d H:i:s";

    /** @var DatabaseConnection */
    protected $dbConnection;

    public function __construct() {
        $this->dbConnection = ServiceManager::getService(DatabaseConnection::class);
    }

    /**
     * Returns currently active trimester
     *
     * @param $date
     * @return mixed|null
     */
    public function getActiveSchoolYear($date = null): SchoolYear {
        if(!$date) {
            $date = date("Y-m-d H:i:s");
        }

        $sql = "SELECT * FROM school_year WHERE start_of_first_quarter < :date AND end_of_fourth_quarter >= :date";
        $prep = $this->dbConnection->getConnection()->prepare($sql);

        $result = $prep->execute([
            'date' => $date
        ]);

        if($result) {
            $obj = $prep->fetchObject();
            if($obj) {
                $schoolYear = new SchoolYear();

                $schoolYear->setId(intval($obj->id));

                $schoolYear->startOfFirstQuarter = \DateTime::createFromFormat(self::DATE_FORMAT, $obj->start_of_first_quarter);
                $schoolYear->endOfFirstQuarter = \DateTime::createFromFormat(self::DATE_FORMAT, $obj->end_of_first_quarter);

                $schoolYear->startOfSecondQuarter = \DateTime::createFromFormat(self::DATE_FORMAT, $obj->start_of_second_quarter);
                $schoolYear->endOfSecondQuarter = \DateTime::createFromFormat(self::DATE_FORMAT, $obj->end_of_second_quarter);

                $schoolYear->startOfThirdQuarter = \DateTime::createFromFormat(self::DATE_FORMAT, $obj->start_of_third_quarter);
                $schoolYear->endOfThirdQuarter = \DateTime::createFromFormat(self::DATE_FORMAT, $obj->end_of_third_quarter);

                $schoolYear->startOfFourthQuarter = \DateTime::createFromFormat(self::DATE_FORMAT, $obj->start_of_fourth_quarter);
                $schoolYear->endOfFourthQuarter = \DateTime::createFromFormat(self::DATE_FORMAT, $obj->end_of_fourth_quarter);

                return $schoolYear;
            }
        }

        throw new \Exception("No active school year found");
    }

}