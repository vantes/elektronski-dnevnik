<?php declare(strict_types = 1);

namespace App\Services;

class ServiceManager {

    /** @var mixed[] */
    private static $services = [];

    public static function getService(string $className) {
        if(!class_exists($className)) {
            throw new \Exception("Ne postoji ovakav servis: " . $className);
        }

        if(isset(self::$services[$className])) {
            return self::$services[$className];
        }

        self::$services[$className] = new $className();
        return self::$services[$className];
    }

    /**
     * Sets a singleton service instance manually
     *
     * @param string $className
     * @param mixed $object
     * @return void
     */
    public static function setService(string $className, $object) {
        if(isset(self::$services[$className])) {
            throw new \Exception("Servis je već pokrenut");
        }
        self::$services[$className] = $object;
    }

}