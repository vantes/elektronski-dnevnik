<?php declare(strict_types = 1);

namespace App\Validators;

use App\Exceptions\ValidationException;
use App\Models\Data\MarkData;
use App\Models\View\TeacherViewModel;
use App\Models\Data\TeacherData;
use App\Models\View\StudentViewModel;
use App\Services\ServiceManager;
use App\Services\Repository\TeacherRepository;

class MarkValidator {

    /** @var TeacherRepository */
    private $teacherRepository;

    public function __construct() {
        $this->teacherRepository = ServiceManager::getService(TeacherRepository::class);
    }

    /**
     * Validates if a final mark can be concluded
     * Throws exception with error otherwise
     *
     * @param TeacherViewModel $teacherViewModel
     * @param StudentViewModel $studentViewModel
     * @param int $subjectId
     * @param int $mark
     * @return void
     */
    public function validateFinalMark(
        TeacherViewModel $teacherViewModel,
        StudentViewModel $studentViewModel,
        $subjectId,
        $mark
    ) {

        $marks = $teacherViewModel->getSubjectMarks($subjectId);
        if(empty($marks)) {
            throw new ValidationException("Ne može se zaključiti krajnja ocena: ne postoje unete ocene");
        }

        if(!$teacherViewModel->checkIfStudentAssigned($studentViewModel->getDataModel()->getId(), $subjectId)) {
            throw new ValidationException('Ne može se zaključiti ocena: učenik ne sluša ovaj predmet kod odabranog nastavnika');
        }

        if($this->teacherRepository->getFinalMark(
            $teacherViewModel->getDataModel()->getId(),
            $studentViewModel->getDataModel()->getId(),
            $subjectId
        ) !== FALSE) {
            throw new ValidationException('Ne može se zaključiti ocena: učenik već ima zaključenu ocenu');
        }

        if($mark < 1 || $mark > 5 || is_float($mark)) {
            throw new ValidationException("Ne može se zaključiti ocena: ocena nije u rasponu od 1-5");
        }
    }

    /**
     * Validates if a subject mark can be assigned
     * Throws exception with error otherwise
     *
     * @param TeacherViewModel $teacherViewModel
     * @param StudentViewModel $studentViewModel
     * @param int $subjectId
     * @param int $mark
     * @return void
     */
    public function validateMark(
        TeacherViewModel $teacherViewModel,
        StudentViewModel $studentViewModel,
        $subjectId,
        $mark
    ) {

        if(!$teacherViewModel->checkIfStudentAssigned($studentViewModel->getDataModel()->getId(), $subjectId)) {
            throw new ValidationException('Ne može se zaključiti ocena: učenik ne sluša ovaj predmet kod odabranog nastavnika');
        }

        if($this->teacherRepository->getFinalMark(
            $teacherViewModel->getDataModel()->getId(),
            $studentViewModel->getDataModel()->getId(),
            $subjectId
        ) !== FALSE) {
            throw new ValidationException('Ne može se zaključiti ocena: učenik već ima zaključenu ocenu');
        }

        if($mark < 1 || $mark > 5 || is_float($mark)) {
            throw new ValidationException("Ne može se dodati ocena: ocena nije u rasponu od 1-5");
        }
    }

    public function validateMarkEntity(
        MarkData $mark
    ) {
        if($mark->getQuarter() < 1 || $mark->getQuarter() > 4) {
            throw new ValidationException('Tromesečje mora biti vrednost između 1-4');
        }

        if($mark->getMark() < 1 || $mark->getMark() > 5) {
            throw new ValidationException('Ocena mora biti vrednost između 1-5');
        }

        if(!$mark->getStudentId()) {
            throw new ValidationException('Ocena mora da sadrži informaciju o učeniku za kojeg se unosi');
        }

        if(!$mark->getTeacherId()) {
            throw new ValidationException('Ocena mora da sadrži informaciju o nastavniku za kojeg se unosi');
        }

        if(!$mark->getSubjectId()) {
            throw new ValidationException('Ocena mora da sadrži informaciju o predmetu za kojeg se unosi');
        }
    }

}