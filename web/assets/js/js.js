
function genderPicture() {
    var user = document.getElementsByClassName("user-gender");
    var gender = null;

    for (var i = 0; i < user.length; i++) {
        var gender = user[i].innerText;
      }

    if (gender == 'M') {
        document.getElementById("profile-picture2").style.display = "none";
    }
    if (gender == 'F') {
        document.getElementById("profile-picture1").style.display = "none";
    }

}

var navSubject = null;
function subjectData(subject) {
    if ( navSubject == null) {
        navSubject = subject;
        document.getElementById(subject).style.display = "block";
        return;
    }

    if ( navSubject != subject) {
        document.getElementById(navSubject).style.display = "none";
        document.getElementById(subject).style.display = "block";
        navSubject = subject;
        return;
    }
}


function goBack() {
  window.history.back();
}
