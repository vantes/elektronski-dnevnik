<?php declare(strict_types = 1);

require_once('vendor/autoload.php');
require_once('Configuration.php');

use App\Core\Bootstrap;

$app = new Bootstrap();
$app->run(Bootstrap::RUN_MODE_DEVELOPMENT);